import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:medsinway/components/AppBarsWrapper/AppBarBottomBarWrapper.dart';
import 'package:medsinway/screens/CompleteProfile/complete_profile_screen.dart';
import 'package:medsinway/screens/HelpCenter/HelpCenter.dart';
import 'package:medsinway/screens/NotVerifiedUser/NotVerifiedUser.dart';
import 'package:medsinway/screens/OfferDetails/OfferDetails.dart';
import 'package:medsinway/screens/OfferUpload/OfferUpload.dart';
import 'package:medsinway/screens/OrderDetails/OrderDetails.dart';
import 'package:medsinway/screens/OrderDetails/components/OrderProgressBar.dart';
import 'package:medsinway/screens/SignIn/sign_in_screen.dart';
import 'package:medsinway/screens/SignUp/sign_up_screen.dart';
import 'package:medsinway/screens/UploadRx/UploadRx.dart';
import 'package:medsinway/screens/splash/SplashLaunchScreen.dart';

import 'components/AppBarsWrapper/AppBarBottomBarWrapper.dart';
import 'screens/ExpressOrder/ExpressOrder.dart';
import 'screens/Home/Home.dart';
import 'screens/Offers/Offers.dart';
import 'screens/Orders/Orders.dart';
import 'screens/Profile/Profile.dart';
import 'screens/splash/splash_screen.dart';

// final Map<String, WidgetBuilder> routes = {
//   Home.routeName: (context) => Home(),
//   MyOrders.routeName: (context) => MyOrders(),
//   // ExpressOrder.routeName: (context) => ExpressOrder(),
//   // ForgotPasswordScreen.routeName: (context) => ForgotPasswordScreen(),
//   // LoginSuccessScreen.routeName: (context) => LoginSuccessScreen(),
//   // SignUpScreen.routeName: (context) => SignUpScreen(),
//   // CompleteProfileScreen.routeName: (context) => CompleteProfileScreen(),
//   // OtpScreen.routeName: (context) => OtpScreen(),
//   // HomeScreen.routeName: (context) => HomeScreen(),
//   // DetailsScreen.routeName: (context) => DetailsScreen(),
//   // CartScreen.routeName: (context) => CartScreen(),
//   // ProfileScreen.routeName: (context) => ProfileScreen(),
// };
class RouteGenerator {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    final args = settings.arguments;
    // final objArgs = settings.arguments as Map["tag"].toString();
    // print(objArgs['order_status']);
    switch (settings.name) {
      case '/':
        return MaterialPageRoute(builder: (context) => Home());
      case '/splash_launch':
        return MaterialPageRoute(builder: (context) => SplashLaunchScreen());
      case '/splash':
        return MaterialPageRoute(builder: (context) => SplashScreen());

      case '/sign_in':
        return MaterialPageRoute(builder: (context) => SignInScreen(args));
      case '/sign_up':
        return MaterialPageRoute(builder: (context) => SignUpScreen());
      case '/not_verified_user':
        return MaterialPageRoute(builder: (context) => NotVerifiedUser());
      case '/complete_profile':
        return MaterialPageRoute(builder: (context) => CompleteProfileScreen());
      case '/express_order':
        return MaterialPageRoute(builder: (context) => ExpressOrder(args));
      case '/upload_Rx':
        return MaterialPageRoute(builder: (context) => UploadRx());
      case '/my_orders':
        return MaterialPageRoute(builder: (context) => Orders());
      case '/order_details':
        return MaterialPageRoute(builder: (context) => OrderDetails(args));
      case '/order_full_status':
        return MaterialPageRoute(builder: (context) => OrderProgressBar(args));
      case '/profile':
        return MaterialPageRoute(builder: (context) => Profile());
      case '/help_center':
        return MaterialPageRoute(builder: (context) => HelpCenter());
      case '/offers':
        return MaterialPageRoute(builder: (context) => Offers());
      case '/offers_upload':
        return MaterialPageRoute(builder: (context) => OfferUpload());
      case '/offer_details':
        return MaterialPageRoute(builder: (context) => OfferDetails(args));

      default:
        return _errorRoute(settings);
    }
  }
}

Route<dynamic> _errorRoute(settings) {
  return MaterialPageRoute(
      builder: (_) => Scaffold(
            body: AppBarBottomBarWrapper(
                child: Text('No route defined for ${settings.name}')),
          ));
}
