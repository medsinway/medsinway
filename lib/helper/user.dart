import 'dart:convert';

import 'package:shared_preferences/shared_preferences.dart';

class User {
  bool isAuth = false;
  var token;
  User();

  Future logIn(authData) async {
    SharedPreferences localStorage = await SharedPreferences.getInstance();
    localStorage.setString('token', json.encode(authData['accessToken']));
    print(json.encode(authData['accessToken']));
    localStorage.setString('user', json.encode(authData['user']));
    if (loadUserData() != null) {
      return true;
      // setState(() {
      //   name = user['fname'];
      // });
    } else {
      return false;
    }
    // Navigator.push(
    //   context,
    //   new MaterialPageRoute(
    //       builder: (context) => Home()
    //   ),
    // );
  }

  getToken() async {
    SharedPreferences localStorage = await SharedPreferences.getInstance();
    token = jsonDecode(localStorage.getString('token'))['token'];
  }

  Future checkIfLoggedIn() async {
    SharedPreferences localStorage = await SharedPreferences.getInstance();
    var token = localStorage.getString('token');
    if (token != null) {
      return true;
    } else {
      return false;
    }
  }

  Future loadUserData() async {
    SharedPreferences localStorage = await SharedPreferences.getInstance();
    var user = localStorage.getString('user');
    print(user);
    if (user != null) {
      return jsonEncode(jsonDecode(user));
      // setState(() {
      //   name = user['fname'];
      // });
    } else {
      return null;
    }
  }

  Future logout() async {
    // var res = await Network().getData('/logout');
    // var body = json.decode(res.body);
    // if (body['success']) {
    SharedPreferences localStorage = await SharedPreferences.getInstance();
    localStorage.remove('user');
    localStorage.remove('token');
    // Navigator.push(context, MaterialPageRoute(builder: (context) => Login()));
    // }

    if (await checkIfLoggedIn() != null) {
      return true;
      // setState(() {
      //   name = user['fname'];
      // });
    } else {
      return false;
    }
  }
}
