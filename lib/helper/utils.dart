import 'package:intl/intl.dart';
import 'package:url_launcher/url_launcher.dart';

launchURL(url) async {
  /// const url = 'https://flutter.dev';
  if (await canLaunch(url)) {
    await launch(url);
  } else {
    throw 'Could not launch $url';
  }
}

toIST(date) {
  DateTime tempDate =
      new DateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").parse(date);

  var inputDate = DateTime.parse(tempDate.toString());
  var outputFormat = DateFormat('dd/MM/yyyy hh:mm a');
  var outputDate = outputFormat.format(inputDate);
  return outputDate;
}
