import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

class Api {
  //enum apiTypeHeaders { homeDeals, homeMainSlider }
  // Local
  // final baseUrl = 'http://192.168.43.72:3000'; //'http://locahost:3001';
  // String _url = 'http://10.0.2.2:3000/v1'; //'http://trymysolution.com/api';
  // String AssetUrl = 'http://192.168.43.72:3000';
  // String Url = 'http://10.0.2.2:3000/v1';

  // Prod

  final baseUrl = 'https://api.medsinway.com'; //'http://locahost:3001';
  String _url =
      'https://api.medsinway.com/v1'; //'http://trymysolution.com/api';
  String AssetUrl = 'https://api.medsinway.com';
  String Url = 'https://api.medsinway.com/v1';

  //if you are using android studio emulator, change localhost to 10.0.2.2
  var token = null;

  updateUrl() {
    _url = '$baseUrl/v1';
    AssetUrl = baseUrl;
    Url = '$baseUrl/v1';
  }

  _getToken() async {
    SharedPreferences localStorage = await SharedPreferences.getInstance();
    print(localStorage.getKeys());

    token = localStorage.getString('token');
    print(token);
    return token;
    print(token);
  }

  tokenProvide() {
    return _getToken();
  }

  postData({dynamic data, apiUrl}) async {
    print('data');
    print(data);
    var fullUrl = _url + apiUrl;
    String tkn = await _getToken();

    return await http.post(fullUrl, body: data, headers: _setHeaders(tkn));
  }

  // duoPost({FormData data, apiUrl}) async {
  //   var fullUrl = _url + apiUrl;
  //
  //   String tkn = await _getToken();
  //
  //   Dio dio = new Dio(BaseOptions(
  //     headers: _setHeaders(tkn),
  //   ));
  //   // /api/order
  //   Response response = await dio.post(fullUrl, data: data);
  //   print('cool');
  //   return response;
  // }

  getData(apiUrl, {headerz}) async {
    var fullUrl = _url + apiUrl;
    String tkn = await _getToken();
    print(fullUrl);
    if (headerz != null) {
      return await http
          .get(fullUrl, headers: {..._setHeaders(tkn), ...headerz});
    } else {
      return await http.get(fullUrl, headers: _setHeaders(tkn));
    }
  }

  _setHeaders(tkn) => {
        'Authorization': "Bearer ${tkn != null ? json.decode(tkn) : ''}",
        "Content-Type": "application/json",
        'Accept': 'application/json',
        'X-Requested-With': 'XMLHttpRequest',
        'Access-Control-Allow-Origin': '*',
        'accessToken': "${tkn != null ? json.decode(tkn) : ''}"
      };
}
