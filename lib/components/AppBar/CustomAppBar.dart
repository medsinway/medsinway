import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:medsinway/constants.dart';
import 'package:medsinway/main.dart';

Widget CustomAppBar(handleCamClick, ctx, {backBtn = false}) {
  IconButton iconMenu() {
    if (backBtn) {
      return IconButton(
        icon: new Icon(Icons.arrow_back),
        onPressed: () => Navigator.of(ctx).pop(),
      );
    }
  }

  return AppBar(
    backgroundColor: kAppColor,
    centerTitle: true,
    title: Text(
      "MEDSINWAY",
      style: TextStyle(
        fontSize: 20,
        color: Color(0xFFFEFEFE),
        fontWeight: FontWeight.bold,
        letterSpacing: 1.5,
      ),
    ),
    automaticallyImplyLeading: !backBtn,
    leading: backBtn ? iconMenu() : null,
    elevation: 10,
    actions: [
      Padding(
        padding: EdgeInsets.all(8.0),
        child: Row(
          //mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            GestureDetector(
              onTap: () {
                handleCamClick();
              },
              child: Column(
                children: [
                  Icon(
                    Icons.camera_alt_outlined,
                    color: Colors.white,
                  ),
                  Text(
                    'Click Rx',
                    style: TextStyle(
                      color: Colors.white,
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(
              width: 10,
            ),
            GestureDetector(
              onTap: () {
                handleCamClick();
              },
              child: Column(
                children: [
                  Icon(
                    Icons.shopping_cart,
                    color: Colors.white,
                  ),
                  StoreConnector<AppState, AppState>(
                    converter: (store) => store.state,
                    builder: (_, state) {
                      return state.counter != null
                          ? Text(
                              'Cart ${state.counter}',
                              style: TextStyle(
                                color: Colors.white,
                              ),
                            )
                          : Text('0');
                    },
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
      // Padding(
      //   padding: EdgeInsets.all(8.0),
      //   child: Icon(Icons.notifications),
      // ),
      // Padding(
      //   padding: EdgeInsets.all(8.0),
      //   child: Icon(Icons.more_vert),
      // ),
    ],
  );
}
