import 'package:flutter/material.dart';

class SnackbarPage extends StatefulWidget {
  bool showSnack;
  String message;
  SnackbarPage(this.showSnack, this.message);
  @override
  _SnackbarPageState createState() => _SnackbarPageState();
}

class _SnackbarPageState extends State<SnackbarPage> {
  @override
  void initState() {
    // TODO: implement initState

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final snackBar = SnackBar(
      content: Text(widget.message),
      //duration: Duration(seconds: 20),
      action: SnackBarAction(
        label: 'Close',
        onPressed: () {
          // Some code to undo the change.
        },
      ),
    );
    // Find the Scaffold in the widget tree and use
    // it to show a SnackBar.
    Future.delayed(Duration.zero, () async {
      if (widget.showSnack == true) {
        ScaffoldMessenger.of(context).showSnackBar(snackBar);

        // Scaffold.of(context).showSnackBar(snackBar);
      }
    });

    return Text(' ');
  }
}
