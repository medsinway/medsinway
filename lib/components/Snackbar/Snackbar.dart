import 'package:flushbar/flushbar_helper.dart';
import 'package:flutter/material.dart';

void CustomeSnackbar(BuildContext context, {title: '', message: ' '}) {
  FlushbarHelper.createInformation(
    title: title,
    message: message,
  ).show(context);
}
