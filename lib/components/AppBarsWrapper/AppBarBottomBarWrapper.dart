import 'dart:io';

import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

import '../AppBar/CustomAppBar.dart';
import '../BottomAppBar/MainBottomAppBar.dart';
import '../Drawer/CustomDrawer.dart';

class AppBarBottomBarWrapper extends StatefulWidget {
  final Widget child;
  String backRoute;
  String currentRoute;
  final Widget bottomBar;
  final bgColor;
  bool enableGoBack = false;

  AppBarBottomBarWrapper(
      {Key key,
      this.child,
      this.backRoute,
      this.currentRoute,
      this.bottomBar,
      this.enableGoBack,
      this.bgColor})
      : super(key: key);

  @override
  _AppBarBottomBarWrapperState createState() => _AppBarBottomBarWrapperState();
}

class _AppBarBottomBarWrapperState extends State<AppBarBottomBarWrapper> {
  // Widget child;
  // _AppBarBottomBarWrapperState(this.child);

  File _image;
  bool isLoading = false;

  final picker = ImagePicker();

  Future getImage() async {
    //_showMyDialog();
    setState(() {
      isLoading = true;
    });
    final pickedFile = await picker.getImage(
      source: ImageSource.camera,
      imageQuality: 80,
    );

    setState(() {
      if (pickedFile != null) {
        _image = File(pickedFile.path);
        setState(() {
          isLoading = false;
        });
        Navigator.pushNamed(context, '/express_order', arguments: _image);
        // Navigator.push(
        //     context,
        //     MaterialPageRoute(
        //       builder: (context) => ExpressOrder(_image),
        //     ));
        //Navigator.pushNamed(context, '/express_order', arguments: _image);
        // _showMyDialog(_image);
        print(_image);
      } else {
        setState(() {
          isLoading = false;
        });
        print('No image selected.');
      }
    });
  }

  Future<void> _showMyDialog(_image) async {
    return showDialog<void>(
      context: context,
      barrierDismissible:
          false, //this means the user must tap a button to exit the Alert Dialog
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('AlertDialog Title'),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                // ExpressOrder(_image),
              ],
            ),
          ),
          actions: <Widget>[
            FlatButton(
              child: Text('Approve'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  // Widget build(BuildContext context) => StoreConnector(
  //     converter: (Store<AppState> store) => _ViewModel.create(store),
  //     builder: (BuildContext context, _ViewModel viewModel) => Scaffold(
  //         appBar: AppBar(
  //           title: Text(viewModel.pageTitle),
  //         ),

  @override
  Widget build(BuildContext context) {
    // bool backBtn = widget.enableGoBack;
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: CustomAppBar(getImage, context, backBtn: widget.enableGoBack),
      backgroundColor: widget.bgColor != null ? Colors.grey : Colors.white,
      drawer: CustomDrawer(),
      body: isLoading ? Center(child: Text('Loading.....')) : widget.child,
      bottomNavigationBar: widget.bottomBar != null
          ? widget.bottomBar
          : CustomBottomNavBar(selectedMenu: widget.currentRoute),
    );
  }
}
