import 'package:flutter/material.dart';
import 'package:medsinway/constants.dart';
import 'package:medsinway/helper/utils.dart';
import 'package:medsinway/screens/Search/Search.dart';

class CustomBottomNavBar extends StatelessWidget {
  const CustomBottomNavBar({
    Key key,
    @required this.selectedMenu,
  }) : super(key: key);

  final String selectedMenu;

  void _settingModalBottomSheet(context) {
    showModalBottomSheet(
        useRootNavigator: true,
        context: context,
        isScrollControlled: true,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
        builder: (BuildContext bc) {
          return Container(
            height: MediaQuery.of(context).size.height * 0.5,
            //padding: EdgeInsets.all(5),
            //margin: EdgeInsets.only(top: 20 + AppBar().preferredSize.height),
            child: Column(
              children: [
                Center(
                  child: Container(
                    width: 150,
                    height: 5,
                    margin: EdgeInsets.only(top: 10),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10.0),
                      color: Colors.blueGrey,
                    ),
                  ),
                ),
                // Align(
                //   alignment: Alignment.topRight,
                //   child: FlatButton(
                //     onPressed: () => Navigator.of(context).pop(),
                //     child: Icon(Icons.close),
                //   ),
                // ),

                Expanded(child: Search()),

                // new Wrap(
                //   children: <Widget>[
                //     new ListTile(
                //         leading: new Icon(Icons.music_note),
                //         title: new Text('Music'),
                //         onTap: () => {}),
                //     new ListTile(
                //       leading: new Icon(Icons.videocam),
                //       title: new Text('Video'),
                //       onTap: () => {},
                //     ),
                //     new ListTile(
                //         leading: new Icon(Icons.music_note),
                //         title: new Text('Music'),
                //         onTap: () => {}),
                //     new ListTile(
                //       leading: new Icon(Icons.videocam),
                //       title: new Text('Video'),
                //       onTap: () => {},
                //     ),
                //     new ListTile(
                //         leading: new Icon(Icons.music_note),
                //         title: new Text('Music'),
                //         onTap: () => {}),
                //     new ListTile(
                //       leading: new Icon(Icons.videocam),
                //       title: new Text('Video'),
                //       onTap: () => {},
                //     ),
                //   ],
                // ),
              ],
            ),
          );
        });
  }

  // Column BottomIcons({icon, color, onClick, text}) {
  //   return Column(
  //     mainAxisSize: MainAxisSize.min,
  //     children: [
  //       IconButton(
  //         icon: Icon(icon, color: color),
  //         onPressed: () => onClick,
  //       ),
  //       Text('text', style: TextStyle(color: color))
  //     ],
  //   );
  // }

  @override
  Widget build(BuildContext context) {
    final Color inActiveIconColor = Color(0xFFB6B6B6);
    return Container(
      padding: EdgeInsets.only(bottom: 6),
      decoration: BoxDecoration(
        color: Colors.white,
        boxShadow: [
          BoxShadow(
            offset: Offset(0, -15),
            blurRadius: 20,
            color: Color(0xFFDADADA).withOpacity(0.15),
          ),
        ],
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(5),
          topRight: Radius.circular(5),
        ),
      ),
      child: SafeArea(
          minimum: EdgeInsets.zero,
          bottom: true,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              // BottomIcons(
              //   color:
              //       'home' == selectedMenu ? kPrimaryColor : inActiveIconColor,
              //   icon: Icons.home,
              //   onClick: Navigator.pushNamed(context, '/'),
              // ),
              Column(
                mainAxisAlignment: MainAxisAlignment.end,
                mainAxisSize: MainAxisSize.min,
                children: [
                  IconButton(
                    padding: EdgeInsets.zero,
                    icon: Icon(
                      Icons.home,
                      color: 'home' == selectedMenu
                          ? kPrimaryColor
                          : inActiveIconColor,
                    ),
                    onPressed: () => Navigator.pushNamed(context, '/'),
                  ),
                  Text('Home')
                ],
              ),
              // IconButton(
              //   icon: Icon(
              //     Icons.home,
              //     color: 'home' == selectedMenu
              //         ? kPrimaryColor
              //         : inActiveIconColor,
              //   ),
              //   onPressed: () => Navigator.pushNamed(context, '/'),
              // ),
              Column(
                mainAxisAlignment: MainAxisAlignment.end,
                mainAxisSize: MainAxisSize.min,
                children: [
                  IconButton(
                    padding: EdgeInsets.zero,
                    icon: Icon(
                      Icons.search,
                      color: 'search' == selectedMenu
                          ? kPrimaryColor
                          : inActiveIconColor,
                    ),
                    onPressed: () => _settingModalBottomSheet(context),
                  ),
                  Text('Search')
                ],
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.end,
                mainAxisSize: MainAxisSize.min,
                children: [
                  IconButton(
                    padding: EdgeInsets.zero,
                    icon: Icon(
                      Icons.local_offer_outlined,
                      color: 'offers' == selectedMenu
                          ? kPrimaryColor
                          : inActiveIconColor,
                    ),
                    onPressed: () => Navigator.pushNamed(context, '/offers'),
                  ),
                  Text('Offers')
                ],
              ),

              // IconButton(
              //   icon: Icon(
              //     Icons.search,
              //     color: 'search' == selectedMenu
              //         ? kPrimaryColor
              //         : inActiveIconColor,
              //   ), // SvgPicture.asset("assets/icons/Heart Icon.svg"),
              //   onPressed: () {},
              // ),
              // IconButton(
              //   icon: Icon(
              //     Icons.local_offer_outlined,
              //     color: 'offers' == selectedMenu
              //         ? kPrimaryColor
              //         : inActiveIconColor,
              //   ), //SvgPicture.asset("assets/icons/Chat bubble Icon.svg"),
              //   onPressed: () => Navigator.pushNamed(context, '/offers'),
              // ),
              Column(
                mainAxisAlignment: MainAxisAlignment.end,
                mainAxisSize: MainAxisSize.min,
                children: [
                  IconButton(
                    padding: EdgeInsets.zero,
                    icon: Icon(
                      Icons.phone,
                      color: 'phone' == selectedMenu
                          ? kPrimaryColor
                          : inActiveIconColor,
                    ),
                    onPressed: () => launchURL(('tel://$kPhoneNumber}')),
                  ),
                  Text('Call')
                ],
              ),
              // IconButton(
              //   icon: Icon(
              //     Icons.phone_in_talk_rounded,
              //     color: 'phone' == selectedMenu
              //         ? kPrimaryColor
              //         : inActiveIconColor,
              //   ),
              //   // SvgPicture.asset(
              //   //   "assets/icons/User Icon.svg",
              //
              //   // ),
              //   onPressed: () => launchURL(('tel://$kPhoneNumber}')),
              // ),
              // IconButton(
              //   icon: Icon(
              //     Icons.shopping_bag,
              //     color: 'orders' == selectedMenu
              //         ? kPrimaryColor
              //         : inActiveIconColor,
              //   ),
              //   onPressed: () => Navigator.pushNamed(context, '/my_orders'),
              //   // SvgPicture.asset(
              //   //   "assets/icons/User Icon.svg",
              //
              //   //),
              // ),
              Column(
                mainAxisAlignment: MainAxisAlignment.end,
                mainAxisSize: MainAxisSize.min,
                children: [
                  IconButton(
                    padding: EdgeInsets.zero,
                    icon: Icon(
                      Icons.shopping_bag,
                      color: 'orders' == selectedMenu
                          ? kPrimaryColor
                          : inActiveIconColor,
                    ),
                    onPressed: () => Navigator.pushNamed(context, '/my_orders'),
                  ),
                  Text('Orders')
                ],
              ),
              // IconButton(
              //   icon: Icon(
              //     Icons.account_circle_outlined,
              //     color: 'profile' == selectedMenu
              //         ? kPrimaryColor
              //         : inActiveIconColor,
              //   ),
              //   // SvgPicture.asset(
              //   //   "assets/icons/User Icon.svg",
              //
              //   // ),
              //   onPressed: () => Navigator.pushNamed(context, '/profile'),
              // ),
              Column(
                mainAxisAlignment: MainAxisAlignment.end,
                mainAxisSize: MainAxisSize.min,
                children: [
                  IconButton(
                    padding: EdgeInsets.zero,
                    icon: Icon(
                      Icons.account_circle_outlined,
                      color: 'profile' == selectedMenu
                          ? kPrimaryColor
                          : inActiveIconColor,
                    ),
                    onPressed: () => Navigator.pushNamed(context, '/profile'),
                  ),
                  Text('Profile')
                ],
              ),
            ],
          )),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.blue,
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(),
      bottomSheet: Container(
          height: 40,
          width: MediaQuery.of(context).size.width,
          child: Center(
              child: Text(
            'semi transparent bottom sheet :)',
            style: TextStyle(color: Colors.white),
          ))),
    );
  }
}
