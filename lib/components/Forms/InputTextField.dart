import 'package:flutter/material.dart';

import '../../constants.dart';

String input;
TextFormField buildTextNumberFormField({
  type: 'text',
  label: '',
  initValue,
  placeholder: 'Enter Input',
  icon,
  onSaved,
  inputName,
  isRequired: true,
  controller,
  minLines: 2,
  maxLines: 2,
}) {
  final List<String> errors = [];

  void addError({String error}) {
    if (!errors.contains(error)) errors.add(error);
    // setState(() {
    //
    // });
  }

  void removeError({String error}) {
    if (errors.contains(error)) errors.remove(error);
    // setState(() {
    //
    // });
  }

  return TextFormField(
    minLines: minLines,
    maxLines: maxLines,
    controller: controller,
    initialValue: initValue,
    cursorHeight: 22,
    keyboardType: (type == 'email' || type == 'phone')
        ? (type == 'email' ? TextInputType.emailAddress : TextInputType.phone)
        : (type == 'multiline' || type == 'text') && type == 'multiline'
            ? TextInputType.multiline
            : TextInputType.text,
    onSaved: (newValue) => input = onSaved(inputName, newValue),
    onChanged: (value) {
      if (isRequired && value.isNotEmpty) {
        removeError(error: kEmailNullError);
      } else if (type == 'email' && emailValidatorRegExp.hasMatch(value)) {
        removeError(error: kInvalidEmailError);
      }
      return null;
    },
    validator: (value) {
      if (isRequired && value.isEmpty) {
        addError(error: kEmailNullError);
        return "";
      } else if (type == 'email' && !emailValidatorRegExp.hasMatch(value)) {
        addError(error: kInvalidEmailError);
        return "";
      }
      return null;
    },
    decoration: InputDecoration(
        labelText: label,
        hintText: placeholder,
        // If  you are using latest version of flutter then lable text and hint text shown like this
        // if you r using flutter less then 1.20.* then maybe this is not working properly
        floatingLabelBehavior: FloatingLabelBehavior.always,
        suffixIcon: icon //CustomSurffixIcon(svgIcon: "assets/icons/Mail.svg"),
        ),
  );
}
