import 'package:flutter/material.dart';
import 'package:medsinway/constants.dart';

class BuildPhoneNumberField extends StatefulWidget {
  @override
  _BuildPhoneNumberFieldState createState() => _BuildPhoneNumberFieldState();
}

class _BuildPhoneNumberFieldState extends State<BuildPhoneNumberField> {
  TextFormField buildPhoneNumberFormField() {
    final List<String> errors = [];
    String phoneNumber;
    void addError({String error}) {
      if (!errors.contains(error))
        setState(() {
          errors.add(error);
        });
    }

    void removeError({String error}) {
      if (errors.contains(error))
        setState(() {
          errors.remove(error);
        });
    }

    return TextFormField(
      keyboardType: TextInputType.phone,
      onSaved: (newValue) => phoneNumber = newValue,
      onChanged: (value) {
        if (value.isNotEmpty) {
          removeError(error: kPhoneNumberNullError);
        }
        return null;
      },
      validator: (value) {
        if (value.isEmpty) {
          addError(error: kPhoneNumberNullError);
          return "";
        }
        return null;
      },
      decoration: InputDecoration(
        labelText: "Phone Number",
        hintText: "Enter your phone number",
        // If  you are using latest version of flutter then lable text and hint text shown like this
        // if you r using flutter less then 1.20.* then maybe this is not working properly
        floatingLabelBehavior: FloatingLabelBehavior.always,
        suffixIcon: Icon(
            Icons.error_outline_outlined), //CustomSurffixIcon(svgIcon: ''),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return buildPhoneNumberFormField();
  }
}
