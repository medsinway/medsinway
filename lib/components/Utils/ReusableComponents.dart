import 'dart:core';

import 'package:flutter/material.dart';

SizedBox CustomeSizeBox({width, height}) {
  if (width is String || width is int) {
    return SizedBox(
      width: double.parse(width),
      height: double.parse(height),
    );
  } else {
    return SizedBox(
      width: width,
      height: height,
    );
  }
}
