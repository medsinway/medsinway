import 'package:flutter/material.dart';
import 'package:medsinway/helper/user.dart';
import 'package:medsinway/screens/SignIn/sign_in_screen.dart';

import '../../screens/Profile/components/ProfilePic.dart';

class CustomDrawer extends StatefulWidget {
  @override
  _CustomDrawerState createState() => _CustomDrawerState();
}

class _CustomDrawerState extends State<CustomDrawer> {
  bool isLogOut;
  void handleLogOut() async {
    if (await User().logout()) {
      setState(() {
        isLogOut = true;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        // Important: Remove any padding from the ListView.
        padding: EdgeInsets.zero,
        children: <Widget>[
          DrawerHeader(
            child: ProfilePic(),
            decoration: BoxDecoration(
              color: Colors.blue,
            ),
          ),
          ListTile(
            title: Text('About Us'),
            onTap: () {
              // Update the state of the app.
              // ...
            },
          ),
          ListTile(
            title: Text('Contact Us'),
            onTap: () {
              // Update the state of the app.
              // ...
            },
          ),
          ListTile(
            title: Text('Terms & Conditions'),
            onTap: () {
              // Update the state of the app.
              // ...
            },
          ),
          ListTile(
            title: Text('Log Out'),
            onTap: () {
              handleLogOut();
              isLogOut ??
                  //Navigator.of(context).pushReplacementNamed('/sign_in');
                  Navigator.pushAndRemoveUntil(
                      context,
                      MaterialPageRoute(
                          builder: (BuildContext context) =>
                              SignInScreen('hideBack')),
                      ModalRoute.withName('/sign_in'));
              // Navigator.pushNamedAndRemoveUntil(context,
              //     SignInScreen.routeName, (Route<dynamic> route) => false
              //);
              // Update the state of the app.
              // ...
            },
          ),
        ],
      ),
    );
  }
}
