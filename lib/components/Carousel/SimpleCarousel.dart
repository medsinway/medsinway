import 'package:flutter/material.dart';

import '../../helper/mapIndexed.dart';

const List imageList = [
  'https://res.cloudinary.com/du8msdgbj/image/upload/w_560,h_240,,a_ignore,q_auto,f_auto/v1609697109/e42avkuozkqhh8u3dh70.png',
  'https://res.cloudinary.com/du8msdgbj/image/upload/w_560,h_240,,a_ignore,q_auto,f_auto/v1609697073/ncuy4b9rnhrwjdwkp6dy.png',
  'https://res.cloudinary.com/du8msdgbj/image/upload/w_560,h_240,,a_ignore,q_auto,f_auto/v1609697374/wf6qw4izw2mi1okmex39.png'
];

class SimpleCarousel extends StatelessWidget {
  double width = 1.059;
  SimpleCarousel({this.width});
  String id = '000';
  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        ...imageList.mapIndexed(
          (e, i) => InkWell(
            onTap: () {
              // Navigator.pushNamed(context, '/offer_details', arguments: id);
            },
            child: Container(
              child: Image.network(
                imageList[1],
                fit: BoxFit.contain,
              ),
              width: MediaQuery.of(context).size.width / 1.059,
              height: 200,
              padding: EdgeInsets.only(left: 10, right: 10),
            ),
          ),
        )
      ],
    );
  }
}
