import 'package:flutter/material.dart';
import 'package:medsinway/helper/Api.dart';
import 'package:medsinway/model/bannersModel.dart';

import '../../helper/mapIndexed.dart';

const List imageList = [
  'https://res.cloudinary.com/du8msdgbj/image/upload/w_560,h_240,,a_ignore,q_auto,f_auto/v1609697109/e42avkuozkqhh8u3dh70.png',
  'https://res.cloudinary.com/du8msdgbj/image/upload/w_560,h_240,,a_ignore,q_auto,f_auto/v1609697073/ncuy4b9rnhrwjdwkp6dy.png',
  'https://res.cloudinary.com/du8msdgbj/image/upload/w_560,h_240,,a_ignore,q_auto,f_auto/v1609697374/wf6qw4izw2mi1okmex39.png'
];

class SimpleGridCarousel extends StatefulWidget {
  List<BannersModel> listOfItems;
  SimpleGridCarousel(this.listOfItems);
  @override
  _SimpleGridCarouselState createState() => _SimpleGridCarouselState();
}

class _SimpleGridCarouselState extends State<SimpleGridCarousel> {
  String id = '000';
  @override
  Widget build(BuildContext context) {
    //listOfItems = listOfItems != null ? listOfItems : [];
    return Row(
      children: [
        ...widget.listOfItems.mapIndexed(
          (listItem, i) => InkWell(
            onTap: () {
              // Navigator.pushNamed(context, '/offer_details', arguments: id);
            },
            child: Card(
              child: Container(
                child: Image.network(
                  Api().AssetUrl + listItem.bannerImage,
                  fit: BoxFit.cover,
                  height: 200,
                ),
                width: MediaQuery.of(context).size.width / 2.2,
                padding: EdgeInsets.symmetric(horizontal: 8, vertical: 12),
              ),
            ),
          ),
        )
      ],
    );
  }
}
