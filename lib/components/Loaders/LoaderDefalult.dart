import 'package:flutter/material.dart';

Center LoaderDefault({text}) {
  return Center(
    child: Text(text != null ? text : 'Loading....'),
  );
}
