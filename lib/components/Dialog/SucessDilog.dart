import 'package:flutter/material.dart';
import 'package:medsinway/constants.dart';

Future<void> sucessDilog(context,
    {msg: '',
    leftBtnTxt: '',
    leftBtnRoute,
    rightBtnTxt: '',
    rightBtnRoute}) async {
  return showDialog<void>(
    context: context,
    barrierDismissible:
        false, //this means the user must tap a button to exit the Alert Dialog
    builder: (BuildContext context) {
      return AlertDialog(
        title: Text(msg),
        content: SingleChildScrollView(
          child: ListBody(
            children: <Widget>[
              // ExpressOrder(_image),
            ],
          ),
        ),
        actions: <Widget>[
          FlatButton(
            child: Text(leftBtnTxt),
            onPressed: () {
              if (leftBtnRoute != null && leftBtnRoute != kCloseTxt) {
                Navigator.pushNamed(context, leftBtnRoute);
              } else if (leftBtnRoute == kCloseTxt) {
                Navigator.pop(context);
              }
            },
          ),
          FlatButton(
            child: Text(rightBtnTxt),
            onPressed: () {
              if (rightBtnRoute != null && rightBtnRoute != kCloseTxt) {
                Navigator.pushNamed(context, rightBtnRoute);
              } else if (rightBtnRoute == kCloseTxt) {
                Navigator.pop(context);
              }
            },
          ),
        ],
      );
    },
  );
}
