enum BottomMenuState {
  home,
  offers,
  phone,
  search,
  orders,
  favourite,
  message,
  profile
}
