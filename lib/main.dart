import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:medsinway/constants.dart';
import 'package:redux/redux.dart';

import 'routes.dart';

void main() => runApp(new MyApp());

@immutable
class AppState {
  final counter;
  final error;
  AppState({this.counter: 0, this.error});
}

//action
enum AppActions { Increment, IncrementError }

//pure function
AppState reducer(AppState prev, action) {
  if (action == AppActions.Increment) {
    print('inc');
    return new AppState(
      counter: prev.counter + 1,
      error: kItemAddedToCart,
    );
  } else if (action == AppActions.IncrementError) {
    return new AppState(error: kSelectQuantity);
  }
  return prev;
}

// import 'screens/Home.dart';
class MyApp extends StatelessWidget {
  final store =
      new Store(reducer, initialState: new AppState(counter: 0, error: ''));
  @override
  Widget build(BuildContext context) {
    return StoreProvider(
      store: store,
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        //home: App(),
        initialRoute: '/splash_launch', //SplashScreen.routeName,
        onGenerateRoute: RouteGenerator.generateRoute,
        // onGenerateRoute: (settings) {
        //   final arguments = settings.arguments;
        //   switch (settings.name) {
        //     case '/express_order':
        //       //if (arguments is String) {
        //       // the details page for one specific user
        //       return ExpressOrder(arguments);
        //     // } else {
        //     // a route showing the list of all users
        //     //return ExpressOrder();
        //     //}
        //     default:
        //       return null;
        //   }
        // },
      ),
    );
  }
}
