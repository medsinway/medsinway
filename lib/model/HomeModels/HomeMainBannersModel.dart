class HomeMainBannersModel {
  int banner_id;
  String banner_heading;
  String banner_details;
  String banner_image;
  String banner_starts_at;
  String banner_ends_at;
  String banner_terms_and_conditions;
  String created_at;
  String updated_at;
  String banner_image_path;

  HomeMainBannersModel({
    this.banner_id,
    this.banner_heading,
    this.banner_details,
    this.banner_image,
    this.banner_starts_at,
    this.banner_ends_at,
    this.banner_terms_and_conditions,
    this.created_at,
    this.updated_at,
    this.banner_image_path,
  });

  factory HomeMainBannersModel.fromJson(Map<String, dynamic> parsedJson) =>
      HomeMainBannersModel(
        banner_id:
            parsedJson['banner_id'] == null ? '' : parsedJson['banner_id'],
        banner_heading: parsedJson['banner_heading'] == null
            ? ''
            : parsedJson['banner_heading'],
        banner_details: parsedJson['banner_details'] == null
            ? ''
            : parsedJson['banner_details'],
        banner_image: parsedJson['banner_image'] == null
            ? ''
            : parsedJson['banner_image'],
        banner_starts_at: parsedJson['banner_starts_at'] == null
            ? ''
            : parsedJson['banner_starts_at'],
        banner_ends_at: parsedJson['banner_ends_at'] == null
            ? ''
            : parsedJson['banner_ends_at'],
        banner_terms_and_conditions:
            parsedJson['banner_terms_and_conditions'] == null
                ? ''
                : parsedJson['banner_terms_and_conditions'],
        created_at:
            parsedJson['created_at'] == null ? '' : parsedJson['created_at'],
        updated_at:
            parsedJson['updated_at'] == null ? '' : parsedJson['updated_at'],
        banner_image_path: parsedJson['banner_image_path'] == null
            ? ''
            : parsedJson['banner_image_path'],
        //source: Source.fromJson(json["source"]),
        // author: json["author"] == null ? null : json["author"],
        // title: json["title"],
        // description: json["description"],
        // url: json["url"],
        // urlToImage: json["urlToImage"],
        // publishedAt: DateTime.parse(json["publishedAt"]),
        // content: json["content"] == null ? null : json["content"],
      );

  Map<String, dynamic> toJson() => {
        //"source": source.toJson(),
        'banner_id': banner_id == null ? null : banner_id,
        banner_heading: banner_heading == null ? null : banner_heading,
        banner_details: banner_details == null ? null : banner_details,
        created_at: created_at == null ? null : created_at,
        banner_image: banner_image == null ? null : banner_image,
        banner_starts_at: banner_starts_at == null ? null : banner_starts_at,
        banner_ends_at: banner_ends_at == null ? null : banner_ends_at,
        banner_terms_and_conditions: banner_terms_and_conditions == null
            ? null
            : banner_terms_and_conditions,
        banner_image_path: banner_image_path == null ? null : banner_image_path,
        updated_at: updated_at == null ? null : updated_at,
        // "author": author == null ? null : author,
        // "title": title,
        // "description": description,
        // "url": url,
        // "urlToImage": urlToImage,
        // "publishedAt": publishedAt.toIso8601String(),
        // "content": content == null ? null : content,
      };
}
