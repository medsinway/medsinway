class DealsModel {
  int deals_id;
  String deals_heading;
  String deals_details;
  String deals_image;
  String banner_starts_at;
  String banner_ends_at;
  String deals_terms_and_conditions;
  String created_at;
  String updated_at;
  String deals_image_path;

  DealsModel({
    this.deals_id,
    this.deals_heading,
    this.deals_details,
    this.deals_image,
    this.banner_starts_at,
    this.banner_ends_at,
    this.deals_terms_and_conditions,
    this.created_at,
    this.updated_at,
    this.deals_image_path,
  });

  factory DealsModel.fromJson(Map<String, dynamic> parsedJson) => DealsModel(
        deals_id: parsedJson['deals_id'] == null ? '' : parsedJson['deals_id'],
        deals_heading: parsedJson['deals_heading'] == null
            ? ''
            : parsedJson['deals_heading'],
        deals_details: parsedJson['deals_details'] == null
            ? ''
            : parsedJson['deals_details'],
        deals_image:
            parsedJson['deals_image'] == null ? '' : parsedJson['deals_image'],
        banner_starts_at: parsedJson['banner_starts_at'] == null
            ? ''
            : parsedJson['banner_starts_at'],
        banner_ends_at: parsedJson['banner_ends_at'] == null
            ? ''
            : parsedJson['banner_ends_at'],
        deals_terms_and_conditions:
            parsedJson['deals_terms_and_conditions'] == null
                ? ''
                : parsedJson['deals_terms_and_conditions'],
        created_at:
            parsedJson['created_at'] == null ? '' : parsedJson['created_at'],
        updated_at:
            parsedJson['updated_at'] == null ? '' : parsedJson['updated_at'],
        deals_image_path: parsedJson['deals_image_path'] == null
            ? ''
            : parsedJson['deals_image_path'],
        //source: Source.fromJson(json["source"]),
        // author: json["author"] == null ? null : json["author"],
        // title: json["title"],
        // description: json["description"],
        // url: json["url"],
        // urlToImage: json["urlToImage"],
        // publishedAt: DateTime.parse(json["publishedAt"]),
        // content: json["content"] == null ? null : json["content"],
      );

  Map<String, dynamic> toJson() => {
        //"source": source.toJson(),
        'deals_id': deals_id == null ? null : deals_id,
        deals_heading: deals_heading == null ? null : deals_heading,
        deals_details: deals_details == null ? null : deals_details,
        created_at: created_at == null ? null : created_at,
        deals_image: deals_image == null ? null : deals_image,
        banner_starts_at: banner_starts_at == null ? null : banner_starts_at,
        banner_ends_at: banner_ends_at == null ? null : banner_ends_at,
        deals_terms_and_conditions: deals_terms_and_conditions == null
            ? null
            : deals_terms_and_conditions,
        deals_image_path: deals_image_path == null ? null : deals_image_path,
        updated_at: updated_at == null ? null : updated_at,
        // "author": author == null ? null : author,
        // "title": title,
        // "description": description,
        // "url": url,
        // "urlToImage": urlToImage,
        // "publishedAt": publishedAt.toIso8601String(),
        // "content": content == null ? null : content,
      };
}
