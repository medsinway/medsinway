class BannersModel {
  //Source source;
  // String author;
  // String title;
  // String description;
  // String url;
  // String urlToImage;
  // DateTime publishedAt;
  // String content;
  //   bannerDetails: "this is details "
  //   bannerHeading: "this is headding"
  //   bannerId: 1
  //   bannerImage: "/uploads/orders/1616926835788-bezkoder-23.jpg"
  //   bannerTermsAndConditions: "terms and cts"
  //   created_at: null
  //   updated_at: null

  int bannerId;
  String bannerHeading;
  String bannerDetails;
  String bannerImage;
  String offer_starts_at;
  String offer_ends_at;
  String bannerTermsAndConditions;
  String created_at;
  String updated_at;
  String bannerImage_path;

  BannersModel({
    // this.source,
    // this.author,
    // this.title,
    // this.description,
    // this.url,
    // this.urlToImage,
    // this.publishedAt,
    // this.content,
    this.bannerId,
    this.bannerHeading,
    this.bannerDetails,
    this.bannerImage,
    this.offer_starts_at,
    this.offer_ends_at,
    this.bannerTermsAndConditions,
    this.created_at,
    this.updated_at,
    this.bannerImage_path,
  });

  // actual_amount: "11"
  // address: "Ayyappnagar"
  // created_at: "2020-06-08T07:21:00.000000Z"
  // offer_percent: "2"
  // order_amount: "122"
  // order_id: 1
  // order_image: null
  // order_name: "wqqw"
  // phone_number: "7654543232"
  // pincode: "560036"
  // prescription_description: "sdfsdv sdvvsdvs sdsd"
  // prescription_image: "48489_Annotation 2020-04-06 122153.png"
  // selling_price: "21"
  // updated_at: "2020-06-08T10:33:14.000000Z"

  factory BannersModel.fromJson(Map<String, dynamic> parsedJson) =>
      BannersModel(
        bannerId: parsedJson['bannerId'] == null ? '' : parsedJson['bannerId'],
        bannerHeading: parsedJson['bannerHeading'] == null
            ? ''
            : parsedJson['bannerHeading'],
        bannerDetails: parsedJson['bannerDetails'] == null
            ? ''
            : parsedJson['bannerDetails'],
        bannerImage:
            parsedJson['bannerImage'] == null ? '' : parsedJson['bannerImage'],
        offer_starts_at: parsedJson['offer_starts_at'] == null
            ? ''
            : parsedJson['offer_starts_at'],
        offer_ends_at: parsedJson['offer_ends_at'] == null
            ? ''
            : parsedJson['offer_ends_at'],
        bannerTermsAndConditions: parsedJson['bannerTermsAndConditions'] == null
            ? ''
            : parsedJson['bannerTermsAndConditions'],
        created_at:
            parsedJson['created_at'] == null ? '' : parsedJson['created_at'],
        updated_at:
            parsedJson['updated_at'] == null ? '' : parsedJson['updated_at'],
        bannerImage_path: parsedJson['bannerImage_path'] == null
            ? ''
            : parsedJson['bannerImage_path'],
        //source: Source.fromJson(json["source"]),
        // author: json["author"] == null ? null : json["author"],
        // title: json["title"],
        // description: json["description"],
        // url: json["url"],
        // urlToImage: json["urlToImage"],
        // publishedAt: DateTime.parse(json["publishedAt"]),
        // content: json["content"] == null ? null : json["content"],
      );

  Map<String, dynamic> toJson() => {
        //"source": source.toJson(),
        'bannerId': bannerId == null ? null : bannerId,
        bannerHeading: bannerHeading == null ? null : bannerHeading,
        bannerDetails: bannerDetails == null ? null : bannerDetails,
        created_at: created_at == null ? null : created_at,
        bannerImage: bannerImage == null ? null : bannerImage,
        offer_starts_at: offer_starts_at == null ? null : offer_starts_at,
        offer_ends_at: offer_ends_at == null ? null : offer_ends_at,
        bannerTermsAndConditions:
            bannerTermsAndConditions == null ? null : bannerTermsAndConditions,
        bannerImage_path: bannerImage_path == null ? null : bannerImage_path,
        updated_at: updated_at == null ? null : updated_at,
        // "author": author == null ? null : author,
        // "title": title,
        // "description": description,
        // "url": url,
        // "urlToImage": urlToImage,
        // "publishedAt": publishedAt.toIso8601String(),
        // "content": content == null ? null : content,
      };
}
