class OrdersModel {
  //Source source;
  // String author;
  // String title;
  // String description;
  // String url;
  // String urlToImage;
  // DateTime publishedAt;
  // String content;

  String actual_amount;
  String address;
  String created_at;
  String offer_percent;
  String order_amount;
  int order_id;
  String order_status;
  String order_image;
  String order_image_path;
  String order_name;
  String phone_number;
  String pincode;
  String prescription_description;
  String prescription_image;
  String selling_price;
  String updated_at;
  String image_path;
  String order_type;
  String prescription_image_path;
  String note;

  OrdersModel(
      {
      // this.source,
      // this.author,
      // this.title,
      // this.description,
      // this.url,
      // this.urlToImage,
      // this.publishedAt,
      // this.content,

      this.actual_amount,
      this.address,
      this.created_at,
      this.offer_percent,
      this.order_amount,
      this.order_id,
      this.order_image,
      this.order_image_path,
      this.order_name,
      this.phone_number,
      this.pincode,
      this.order_status,
      this.prescription_description,
      this.prescription_image_path,
      this.prescription_image,
      this.selling_price,
      this.updated_at,
      this.image_path,
      this.order_type,
      this.note});

  // actual_amount: "11"
  // address: "Ayyappnagar"
  // created_at: "2020-06-08T07:21:00.000000Z"
  // offer_percent: "2"
  // order_amount: "122"
  // order_id: 1
  // order_image: null
  // order_name: "wqqw"
  // phone_number: "7654543232"
  // pincode: "560036"
  // prescription_description: "sdfsdv sdvvsdvs sdsd"
  // prescription_image: "48489_Annotation 2020-04-06 122153.png"
  // selling_price: "21"
  // updated_at: "2020-06-08T10:33:14.000000Z"

  factory OrdersModel.fromJson(Map<String, dynamic> parsedJson) => OrdersModel(
      actual_amount: parsedJson["actual_amount"] == null
          ? null
          : parsedJson["actual_amount"],
      order_type:
          parsedJson["order_type"] == null ? null : parsedJson["order_type"],
      order_status: parsedJson["order_status"] == null
          ? null
          : parsedJson["order_status"],
      address: parsedJson["address"] == null ? null : parsedJson["address"],
      created_at:
          parsedJson["created_at"] == null ? null : parsedJson["created_at"],
      offer_percent: parsedJson["offer_percent"] == null
          ? null
          : parsedJson["offer_percent"],
      order_amount: parsedJson["order_amount"] == null
          ? null
          : parsedJson["order_amount"],
      order_id: parsedJson["order_id"] == null ? null : parsedJson["order_id"],
      order_image:
          parsedJson["order_image"] == null ? null : parsedJson["order_image"],
      order_image_path: parsedJson["order_image_path"] == null
          ? null
          : parsedJson["order_image_path"],
      order_name:
          parsedJson["order_name"] == null ? null : parsedJson["order_name"],
      phone_number: parsedJson["phone_number"] == null
          ? null
          : parsedJson["phone_number"],
      pincode: parsedJson["pincode"] == null ? null : parsedJson["pincode"],
      prescription_description: parsedJson["prescription_description"] == null
          ? null
          : parsedJson["prescription_description"],
      prescription_image: parsedJson["prescription_image"] == null
          ? null
          : parsedJson["prescription_image"],
      prescription_image_path: parsedJson["prescription_image_path"] == null
          ? null
          : parsedJson["prescription_image_path"],
      selling_price: parsedJson["selling_price"] == null
          ? null
          : parsedJson["selling_price"],
      updated_at:
          parsedJson["updated_at"] == null ? null : parsedJson["updated_at"],
      image_path:
          parsedJson['image_path'] == null ? '' : parsedJson['image_path'],
      note: parsedJson['note'] == null ? '' : parsedJson['note']

      //source: Source.fromJson(json["source"]),
      // author: json["author"] == null ? null : json["author"],
      // title: json["title"],
      // description: json["description"],
      // url: json["url"],
      // urlToImage: json["urlToImage"],
      // publishedAt: DateTime.parse(json["publishedAt"]),
      // content: json["content"] == null ? null : json["content"],
      );

  Map<String, dynamic> toJson() => {
        //"source": source.toJson(),
        actual_amount: actual_amount == null ? null : actual_amount,
        order_type: order_type == null ? null : order_type,
        address: address == null ? null : address,
        created_at: created_at == null ? null : created_at,
        offer_percent: offer_percent == null ? null : offer_percent,
        order_amount: order_amount == null ? null : order_amount,
        'order_id': order_id == null ? 0 : order_id,
        'order_image': order_image == null ? null : order_image,
        order_name: order_name == null ? null : order_name,
        phone_number: phone_number == null ? null : phone_number,
        pincode: pincode == null ? null : pincode,
        prescription_description:
            prescription_description == null ? null : prescription_description,
        prescription_image:
            prescription_image == null ? null : prescription_image,
        selling_price: selling_price == null ? null : selling_price,
        updated_at: updated_at == null ? null : updated_at,
        image_path: image_path == null ? '' : image_path,
        note: note == null ? '' : note
        // "author": author == null ? null : author,
        // "title": title,
        // "description": description,
        // "url": url,
        // "urlToImage": urlToImage,
        // "publishedAt": publishedAt.toIso8601String(),
        // "content": content == null ? null : content,
      };
}
