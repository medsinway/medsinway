class OffersModel {
//Source source;
// String author;
// String title;
// String description;
// String url;
// String urlToImage;
// DateTime publishedAt;
// String content;

  int offer_id;
  String offer_heading;
  String offer_details;
  String offer_image;
  String offer_starts_at;
  String offer_ends_at;
  String offer_terms_and_conditions;
  String created_at;
  String updated_at;
  String offer_image_path;

  OffersModel({
    // this.source,
    // this.author,
    // this.title,
    // this.description,
    // this.url,
    // this.urlToImage,
    // this.publishedAt,
    // this.content,
    this.offer_id,
    this.offer_heading,
    this.offer_details,
    this.offer_image,
    this.offer_starts_at,
    this.offer_ends_at,
    this.offer_terms_and_conditions,
    this.created_at,
    this.updated_at,
    this.offer_image_path,
  });

// actual_amount: "11"
// address: "Ayyappnagar"
// created_at: "2020-06-08T07:21:00.000000Z"
// offer_percent: "2"
// order_amount: "122"
// order_id: 1
// order_image: null
// order_name: "wqqw"
// phone_number: "7654543232"
// pincode: "560036"
// prescription_description: "sdfsdv sdvvsdvs sdsd"
// prescription_image: "48489_Annotation 2020-04-06 122153.png"
// selling_price: "21"
// updated_at: "2020-06-08T10:33:14.000000Z"

  factory OffersModel.fromJson(Map<String, dynamic> parsedJson) => OffersModel(
        offer_id: parsedJson['offer_id'] == null ? '' : parsedJson['offer_id'],
        offer_heading: parsedJson['offer_heading'] == null
            ? ''
            : parsedJson['offer_heading'],
        offer_details: parsedJson['offer_details'] == null
            ? ''
            : parsedJson['offer_details'],
        offer_image:
            parsedJson['offer_image'] == null ? '' : parsedJson['offer_image'],
        offer_starts_at: parsedJson['offer_starts_at'] == null
            ? ''
            : parsedJson['offer_starts_at'],
        offer_ends_at: parsedJson['offer_ends_at'] == null
            ? ''
            : parsedJson['offer_ends_at'],
        offer_terms_and_conditions:
            parsedJson['offer_terms_and_conditions'] == null
                ? ''
                : parsedJson['offer_terms_and_conditions'],
        created_at:
            parsedJson['created_at'] == null ? '' : parsedJson['created_at'],
        updated_at:
            parsedJson['updated_at'] == null ? '' : parsedJson['updated_at'],
        offer_image_path: parsedJson['offer_image_path'] == null
            ? ''
            : parsedJson['offer_image_path'],
        //source: Source.fromJson(json["source"]),
        // author: json["author"] == null ? null : json["author"],
        // title: json["title"],
        // description: json["description"],
        // url: json["url"],
        // urlToImage: json["urlToImage"],
        // publishedAt: DateTime.parse(json["publishedAt"]),
        // content: json["content"] == null ? null : json["content"],
      );

  Map<String, dynamic> toJson() => {
        //"source": source.toJson(),
        'offer_id': offer_id == null ? null : offer_id,
        offer_heading: offer_heading == null ? null : offer_heading,
        offer_details: offer_details == null ? null : offer_details,
        created_at: created_at == null ? null : created_at,
        offer_image: offer_image == null ? null : offer_image,
        offer_starts_at: offer_starts_at == null ? null : offer_starts_at,
        offer_ends_at: offer_ends_at == null ? null : offer_ends_at,
        offer_terms_and_conditions: offer_terms_and_conditions == null
            ? null
            : offer_terms_and_conditions,
        offer_image_path: offer_image_path == null ? null : offer_image_path,
        updated_at: updated_at == null ? null : updated_at,
        // "author": author == null ? null : author,
        // "title": title,
        // "description": description,
        // "url": url,
        // "urlToImage": urlToImage,
        // "publishedAt": publishedAt.toIso8601String(),
        // "content": content == null ? null : content,
      };
}
