import 'package:flutter/material.dart';

class SizeConfig {
  static MediaQueryData _mediaQueryData;
  static double screenWidth;
  static double screenHeight;
  static double defaultSize;
  static Orientation orientation;

  void init(BuildContext context) {
    _mediaQueryData = MediaQuery.of(context);
    screenWidth = _mediaQueryData.size.width;
    screenHeight = _mediaQueryData.size.height;
    orientation = _mediaQueryData.orientation;
  }
}

// Get the proportionate height as per screen size
double getProportionateScreenHeight(double inputHeight) {
  double screenHeight = SizeConfig.screenHeight;
  // 812 is the layout height that designer use
  return (inputHeight / 812.0) * screenHeight;
}

// Get the proportionate height as per screen size
double getProportionateScreenWidth(double inputWidth) {
  double screenWidth = SizeConfig.screenWidth;
  // 375 is the layout width that designer use
  // boxHeight = MediaQuery.of(context).size.height / 100;
  // boxWidth = MediaQuery.of(context).size.width / 100;
  return inputWidth != null && screenWidth != null
      ? (inputWidth / 375.0) * screenWidth
      : inputWidth / 735.0 * screenWidth;
}

class Dimensions {
  static double boxWidth;
  static double boxHeight;

  Dimensions(context) {
    boxHeight = MediaQuery.of(context).size.height / 100;
    boxWidth = MediaQuery.of(context).size.width / 100;
  }
}

double getProportionateScreenWidthCtx(double inputWidth, ctx) {
  double screenWidth = MediaQuery.of(ctx).size.width;
  // 375 is the layout width that designer use
  // boxHeight = MediaQuery.of(context).size.height / 100;
  // boxWidth = MediaQuery.of(context).size.width / 100;
  return inputWidth != null && screenWidth != null
      ? (inputWidth / 375.0) * screenWidth
      : inputWidth / 735 * screenWidth;
}

double getProportionateScreenHeightCtx(double inputHeight, ctx) {
  double screenHeight = MediaQuery.of(ctx).size.height;
  ;
  // 812 is the layout height that designer use
  return (inputHeight / 812.0) * screenHeight;
}
