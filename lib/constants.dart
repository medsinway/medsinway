import 'package:flutter/material.dart';
import 'package:medsinway/size_config.dart';

const kPrimaryColor = Color(0xFFFF7643);
const kAppColor = Color(0xFF017b7e);
const kPrimaryLightColor = Color(0xFFFFECDF);
const kPrimaryGradientColor = LinearGradient(
  begin: Alignment.topLeft,
  end: Alignment.bottomRight,
  colors: [Color(0xFFFFA53E), Color(0xFFFF7643)],
);
const kSecondaryColor = Color(0xFF979797);
const kTextColor = Color(0xFF757575);
const kPhoneNumber = '8050507766';
const kEmail = 'medsinway@outlook.com';

// Form Error
final RegExp emailValidatorRegExp =
    RegExp(r"^[a-zA-Z0-9.]+@[a-zA-Z0-9]+\.[a-zA-Z]+");
final RegExp phoneNumberValidate = RegExp(r"^([9]{1})([234789]{1})([0-9]{8})$");
const String kEmailNullError = "Please Enter your email";
const String kInvalidEmailError = "Please Enter Valid Email";
const String kPassNullError = "Please Enter your password";
const String kShortPassError = "Password is too short";
const String kMatchPassError = "Passwords don't match";
const String kNamelNullError = "Please Enter your name";
const String kPhoneNumberNullError = "Please Enter your phone number";
const String kAddressNullError = "Please Enter your address";
const String kPrivacyPolicy =
    "By continuing you confirm that you agree \nto our Privacy Policy and Terms and Conditions";
const String kUploadPrescriptionSnack =
    'Please Upload Prescription to Continue Ahead';
const int kMaxItemCount = 100;
const String kSelectQuantity = 'Please select at least One Quantity';
const String kItemAddedToCart = 'Item added to Cart';

const kAnimationDuration = Duration(milliseconds: 200);

final headingStyle = TextStyle(
  fontSize: 20,
  fontWeight: FontWeight.bold,
  color: Colors.black,
  height: 1.5,
);

const defaultDuration = Duration(milliseconds: 250);

final otpInputDecoration = InputDecoration(
  contentPadding:
      EdgeInsets.symmetric(vertical: getProportionateScreenWidth(15)),
  border: outlineInputBorder(),
  focusedBorder: outlineInputBorder(),
  enabledBorder: outlineInputBorder(),
);

OutlineInputBorder outlineInputBorder() {
  return OutlineInputBorder(
    borderRadius: BorderRadius.circular(getProportionateScreenWidth(15)),
    borderSide: BorderSide(color: kTextColor),
  );
}

const khomeDealsHeader = {'bannerType': 'homeDeals'};
const khomeMainSliderHeader = {'bannerType': 'homeMainSlider'};
const khomeOffersPage = {'bannerType': 'offersPage'};
const khomeDeals = 'homeDeals';
const khomeMainSlider = 'homeMainSlider';
const kbannerType = 'bannerType';
const kOrderSucessMsg = 'Your order has been placed';
const kMyOrdersTxt = 'My Orders';
const kHomeTxt = 'Home';
const kOffersPageTxt = 'Offers Page';
const kCloseTxt = 'Close';
const kOfferCreateSucessMsg = 'Offer has been Created';
