// import 'package:blog_app/models/ImageUploadModel.dart';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

class ImageUploadModel {
  bool isUploaded;
  bool uploading;
  File imageFile;
  String imageUrl;

  ImageUploadModel({
    this.isUploaded,
    this.uploading,
    this.imageFile,
    this.imageUrl,
  });
}

class MultipleImageUpload extends StatefulWidget {
  Function callBackImage;
  MultipleImageUpload(this.callBackImage);
  @override
  State<StatefulWidget> createState() {
    return MultipleImageUploadState();
  }
}

class MultipleImageUploadState extends State<MultipleImageUpload> {
  static List<Object> images = List<Object>();
  File _imageFile;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    setState(() {
      if (images.length == 0) {
        images.add("Add Image");
      }

      // images.add("Add Image");
      // images.add("Add Image");
      // images.add("Add Image");
    });
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Expanded(
          child: buildGridView(),
        ),
      ],
    );
  }

  Widget buildGridView() {
    return GridView.count(
      shrinkWrap: true,
      crossAxisCount: 3,
      childAspectRatio: 1,
      children: List.generate(
        images.length,
        (index) {
          if (images[index] is ImageUploadModel) {
            ImageUploadModel uploadModel = images[index];
            return Card(
              shape: RoundedRectangleBorder(
                side: new BorderSide(color: Colors.grey, width: 1.0),
                borderRadius: BorderRadius.circular(4.0),
              ),
              clipBehavior: Clip.antiAlias,
              child: Stack(
                children: <Widget>[
                  Image.file(
                    uploadModel.imageFile,
                    width: 300,
                    height: 300,
                  ),
                  Positioned(
                    right: 5,
                    top: 5,
                    child: InkWell(
                      child: Icon(
                        Icons.remove_circle,
                        size: 20,
                        color: Colors.red,
                      ),
                      onTap: () {
                        setState(() {
                          images.removeAt(index);
                          widget.callBackImage(images);
                          //images.replaceRange(index, index + 1, ['Add Image']);
                        });
                      },
                    ),
                  ),
                ],
              ),
            );
          } else {
            return Card(
              shape: RoundedRectangleBorder(
                side: new BorderSide(color: Colors.blue, width: 4.0),
                borderRadius: BorderRadius.circular(4.0),
              ),
              child: IconButton(
                icon: Icon(
                  Icons.upload_file,
                  color: Colors.blue,
                  size: 60,
                ),
                onPressed: () {
                  _onAddImageClick(index);
                },
              ),
            );
          }
        },
      ),
    );
  }

  final picker = ImagePicker();

  Future _onAddImageClick(int index) async {
    //dynamic img = await ImagePicker().getImage(source: ImageSource.gallery);
    // setState(() {
    //   _imageFile =
    //       img; // await ImagePicker().getImage(source: ImageSource.gallery);
    //   ; //ImagePicker.pickImage(source: ImageSource.gallery);
    //
    // });

    final pickedFile = await picker.getImage(source: ImageSource.gallery);
    print(pickedFile.toString());

    setState(() {
      if (pickedFile != null) {
        _imageFile = File(pickedFile.path);
        //getFileImage(index);
        ImageUploadModel imageUpload = new ImageUploadModel();
        imageUpload.isUploaded = false;
        imageUpload.uploading = false;
        imageUpload.imageFile = File(pickedFile.path);
        imageUpload.imageUrl = '';
        images.replaceRange(index, index + 1, [imageUpload]);
        images.add("Add Image");
        widget.callBackImage(images);
      } else {
        print('No image selected.');
      }
    });
  }

//   void getFileImage(int index) async {
// //    var dir = await path_provider.getTemporaryDirectory();
//
//     _imageFile.then((file) async {
//       setState(() {});
//     });
//   }
}
