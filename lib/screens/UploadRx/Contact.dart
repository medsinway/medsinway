import 'package:flutter/material.dart';
import 'package:medsinway/components/Forms/InputTextField.dart';

class Contact extends StatefulWidget {
  Function onContinue;
  Contact(this.onContinue);
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return ContactState();
  }
}

class ContactState extends State<Contact> {
  static final formKey = GlobalKey<FormState>();
  static TextEditingController controllerName =
      new TextEditingController(text: '');
  static TextEditingController controllerEmail = new TextEditingController();
  static TextEditingController controllerAddress = new TextEditingController();
  static TextEditingController controllerPhoneNumber =
      new TextEditingController();
  static TextEditingController controllerAge = new TextEditingController();
  static TextEditingController controllerGender = new TextEditingController();
  static TextEditingController controllerNotes = new TextEditingController();

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
        child: Form(
      key: formKey,
      autovalidate: false,
      child: Column(
        children: <Widget>[
          buildTextNumberFormField(
            // maxLines: 1,
            controller: controllerName,
            type: 'text',
            placeholder: 'Enter Your Name',
            inputName: 'name',
          ),
          // TextFormField(
          //   maxLines: 1,
          //   controller: controllerName,
          //   decoration: InputDecoration(
          //     prefixIcon: const Icon(
          //       Icons.person,
          //       color: Colors.grey,
          //     ),
          //     hintText: 'Name',
          //     border: OutlineInputBorder(
          //       borderRadius: BorderRadius.all(Radius.circular(10.0)),
          //     ),
          //   ),
          //   validator: (value) {
          //     // if (value.trim().isEmpty) {
          //     //   return "First Name is Required";
          //     // }
          //   },
          // ),
          SizedBox(
            height: 20,
          ),
          buildTextNumberFormField(
            // maxLines: 1,
            controller: controllerPhoneNumber,
            type: 'phone',
            placeholder: 'Enter Your Mobile No',
            inputName: 'phone',
          ),
          // TextFormField(
          //   maxLines: 1,
          //   decoration: InputDecoration(
          //     prefixIcon: const Icon(
          //       Icons.phone,
          //       color: Colors.grey,
          //     ),
          //     hintText: 'Mobile No',
          //     border: OutlineInputBorder(
          //       borderRadius: BorderRadius.all(Radius.circular(10.0)),
          //     ),
          //   ),
          //   validator: (value) {
          //     if (value.trim().isEmpty) {
          //       return "Mobile No is Required";
          //     }
          //   },
          //   controller: controllerPhoneNumber,
          // ),
          SizedBox(
            height: 20,
          ),
          buildTextNumberFormField(
            // maxLines: 1,
            controller: controllerEmail,
            type: 'email',
            placeholder: 'Enter Your Email',
            inputName: 'email',
          ),
          // TextFormField(
          //   maxLines: 1,
          //   decoration: InputDecoration(
          //     prefixIcon: const Icon(
          //       Icons.email,
          //       color: Colors.grey,
          //     ),
          //     hintText: 'Email',
          //     border: OutlineInputBorder(
          //       borderRadius: BorderRadius.all(Radius.circular(10.0)),
          //     ),
          //   ),
          //   validator: (value) {
          //     // if (value.trim().isEmpty) {
          //     //   return "Email is Required";
          //     // }
          //   },
          //   controller: controllerEmail,
          // ),
          SizedBox(height: 20),
          buildTextNumberFormField(
            minLines: 3,
            maxLines: 7,
            controller: controllerNotes,
            type: 'multiline',
            placeholder: 'Enter Your Notes',
            inputName: 'notes',
            icon: Icon(
              Icons.sticky_note_2_outlined,
              color: Colors.grey,
              size: 33,
            ),
          ),
          // TextFormField(
          //   minLines: 3,
          //   maxLines: 7,
          //   decoration: InputDecoration(
          //     prefixIcon: const Icon(
          //       Icons.sticky_note_2_outlined,
          //       color: Colors.grey,
          //       size: 33,
          //     ),
          //     hintText: 'Notes',
          //     border: OutlineInputBorder(
          //       borderRadius: BorderRadius.all(Radius.circular(10.0)),
          //     ),
          //   ),
          //   controller: controllerNotes,
          // ),
          SizedBox(height: 20),
          buildTextNumberFormField(
            minLines: 5,
            maxLines: 7,
            controller: controllerAddress,
            type: 'multiline',
            placeholder: 'Enter Your Address',
            inputName: 'address',
            icon: Icon(
              Icons.home,
              color: Colors.grey,
            ),
          ),
          // TextFormField(
          //   minLines: 5,
          //   maxLines: 7,
          //   decoration: InputDecoration(
          //     prefixIcon: const Icon(
          //       Icons.home,
          //       color: Colors.grey,
          //     ),
          //     hintText: 'Address',
          //     border: OutlineInputBorder(
          //       borderRadius: BorderRadius.all(Radius.circular(10.0)),
          //     ),
          //   ),
          //   controller: controllerAddress,
          // ),
          SizedBox(
            height: 20,
          ),
          buildTextNumberFormField(
            // maxLines: 1,
            controller: controllerAge,
            type: 'phone',
            placeholder: 'Enter Your Age',
            inputName: 'age',
          ),
          // TextFormField(
          //   maxLines: 1,
          //   decoration: InputDecoration(
          //     prefixIcon: const Icon(
          //       Icons.perm_contact_calendar,
          //       color: Colors.grey,
          //     ),
          //     hintText: 'Age',
          //     border: OutlineInputBorder(
          //       borderRadius: BorderRadius.all(Radius.circular(10.0)),
          //     ),
          //   ),
          //   validator: (value) {
          //     // if (value.trim().isEmpty) {
          //     //   return "Mobile No is Required";
          //     // }
          //   },
          //   controller: controllerAge,
          // ),
          SizedBox(height: 20),
          buildTextNumberFormField(
            // maxLines: 1,
            controller: controllerGender,
            type: 'text',
            placeholder: 'Enter Your Gender',
            inputName: 'gender',
          ),
          // TextFormField(
          //   maxLines: 1,
          //   decoration: InputDecoration(
          //     prefixIcon: const Icon(
          //       Icons.person,
          //       color: Colors.grey,
          //     ),
          //     hintText: 'Gender',
          //     border: OutlineInputBorder(
          //       borderRadius: BorderRadius.all(Radius.circular(10.0)),
          //     ),
          //   ),
          //   controller: controllerGender,
          // ),
          SizedBox(
            height: 10,
          ),
          Center(
            child: FlatButton(
              onPressed: () => widget.onContinue(),
              child: Text('Continue'),
              textColor: Colors.white,
              color: Colors.blue,
            ),
          ),
          SizedBox(
            height: 20,
          ),
        ],
      ),
    ));
  }
}
