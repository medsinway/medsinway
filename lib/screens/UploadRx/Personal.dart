import 'package:flutter/material.dart';
import 'package:medsinway/screens/UploadRx/components/UploadMultipleImages.dart';

import '../../helper/mapIndexed.dart';

class Personal extends StatefulWidget {
  Function onContinue;
  Personal(this.onContinue);
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return PersonalState();
  }
}

class PersonalState extends State<Personal> {
  // Function onContinue;
  // PersonalState(this.onContinue);
  bool isValidImage = false;
  static final formKey = GlobalKey<FormState>();
  static TextEditingController controllerFirstName =
      new TextEditingController();
  static TextEditingController controllerLastName = new TextEditingController();
  static TextEditingController controllerAge = new TextEditingController();
  static TextEditingController controllerGender = new TextEditingController();

  ListTile customListTile(text) {
    return ListTile(
      visualDensity: VisualDensity(horizontal: 0, vertical: -4),
      contentPadding: EdgeInsets.all(0),
      leading: Icon(
        Icons.fiber_manual_record,
        size: 16,
      ),
      title: Transform(
          transform: Matrix4.translationValues(-22, 0.0, 0.0),
          child: Text(text)),
    );
  }

  Row validPrescription() {
    List imgList = [
      {'img': 'assets/images/doc-details.png', 'text': 'Doctor \nDetails'},
      {'img': 'assets/images/date.png', 'text': 'Prescription \nDate'},
      {'img': 'assets/images/pateint-details.png', 'text': 'Patient \nDetails'},
      {'img': 'assets/images/medicine.png', 'text': 'Dosage \nDetails'},
    ];

    return Row(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        ...imgList.mapIndexed(
          (orderItem, i) => Column(
            // mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Image.asset(
                orderItem['img'],
                width: 30,
              ),
              Center(
                child: Text(
                  orderItem['text'],
                  style: TextStyle(fontSize: 14),
                  textAlign: TextAlign.center,
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }

  void callBackImage(images) {
    setState(() {
      if (images.length > 1) {
        isValidImage = true;
      } else {
        isValidImage = false;
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Center(
              child: isValidImage
                  ? null
                  : Text(
                      'Image Required',
                      style: TextStyle(color: Colors.red),
                    ),
            ),
            SizedBox(
              height: 10,
            ),
            SizedBox(
              height: 170,
              width: MediaQuery.of(context).size.width,
              child: MultipleImageUpload(callBackImage),
            ),
            SizedBox(
              height: 5,
            ),
            Center(
              child: FlatButton(
                onPressed: () => widget.onContinue(),
                child: Text('Continue'),
                textColor: Colors.white,
                color: Colors.blue,
              ),
            ),
            SizedBox(
              height: 20,
            ),
            Text(
              'What is a Valid Prescription?',
              style: TextStyle(color: Color(0xff0086ff)),
              textAlign: TextAlign.left,
            ),
            SizedBox(
              height: 15,
            ),
            SizedBox(
              height: 100,
              width: MediaQuery.of(context).size.width,
              child: validPrescription(),
            ),
            Text(
              'Prescription Guide',
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
              textAlign: TextAlign.left,
            ),
            Column(
              children: <Widget>[
                customListTile('Upload Clear Images, Avoid Blurred One'),
                customListTile('Don\'t Crop Out Any Part of the Prescription'),
                customListTile('Don\'t Upload Pictures of the Medicines'),
              ],
            )

            // Container(
            //     child: Form(
            //   key: formKey,
            //   autovalidate: false,
            //   child: Column(
            //     children: <Widget>[
            //       TextFormField(
            //         maxLines: 1,
            //         controller: controllerFirstName,
            //         decoration: InputDecoration(
            //           prefixIcon: const Icon(
            //             Icons.person,
            //             color: Colors.grey,
            //           ),
            //           hintText: 'First Name',
            //           border: OutlineInputBorder(
            //             borderRadius: BorderRadius.all(Radius.circular(10.0)),
            //           ),
            //         ),
            //         validator: (value) {
            //           if (value.trim().isEmpty) {
            //             return "First Name is Required";
            //           }
            //         },
            //       ),
            //       SizedBox(height: 20),
            //       TextFormField(
            //           maxLines: 1,
            //           decoration: InputDecoration(
            //             prefixIcon: const Icon(
            //               Icons.person,
            //               color: Colors.grey,
            //             ),
            //             hintText: 'Last Name',
            //             border: OutlineInputBorder(
            //               borderRadius: BorderRadius.all(Radius.circular(10.0)),
            //             ),
            //           ),
            //           validator: (value) {
            //             if (value.trim().isEmpty) {
            //               return "Last Name is Required";
            //             }
            //           },
            //           controller: controllerLastName),
            //       SizedBox(height: 20),
            //       TextFormField(
            //           maxLines: 1,
            //           decoration: InputDecoration(
            //             prefixIcon: const Icon(
            //               Icons.calendar_today,
            //               color: Colors.grey,
            //             ),
            //             hintText: 'Date of Birth',
            //             border: OutlineInputBorder(
            //               borderRadius: BorderRadius.all(Radius.circular(10.0)),
            //             ),
            //           ),
            //           controller: controllerDateOfBirth),
            //       SizedBox(height: 20),
            //       TextFormField(
            //           maxLines: 1,
            //           decoration: InputDecoration(
            //             prefixIcon: const Icon(
            //               Icons.person,
            //               color: Colors.grey,
            //             ),
            //             hintText: 'Gender',
            //             border: OutlineInputBorder(
            //               borderRadius: BorderRadius.all(Radius.circular(10.0)),
            //             ),
            //           ),
            //           controller: controllerGender),
            //     ],
            //   ),
            // )),
          ],
        ),
      ),
    );
  }
}
