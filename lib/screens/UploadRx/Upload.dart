import 'dart:collection';

import 'package:flutter/material.dart';
import 'package:medsinway/screens/UploadRx/components/UploadMultipleImages.dart';

class Upload extends StatefulWidget {
  var mapInfo = HashMap<String, String>();
  Function onContinue;

  List<Object> images = MultipleImageUploadState.images;

  Upload(this.mapInfo, this.images, this.onContinue);

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return UploadState();
  }
}

class UploadState extends State<Upload> {
  @override
  void initState() {
    // print(json.decode(widget.images[0]));
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var name = widget.mapInfo["name"];
    var age = widget.mapInfo["age"];
    var gender = widget.mapInfo["gender"];
    var email = widget.mapInfo["email"];
    var address = widget.mapInfo["address"];
    var phoneNumber = widget.mapInfo["phone_number"];
    var notes = widget.mapInfo["note"];

    // TODO: implement build
    return Container(
      child: Column(
        children: <Widget>[
          GridView.count(
            shrinkWrap: true,
            crossAxisCount: 3,
            childAspectRatio: 1,
            children: List.generate(widget.images.length, (index) {
              if (widget.images[index] is ImageUploadModel) {
                ImageUploadModel uploadModel = widget.images[index];
                return Card(
                  clipBehavior: Clip.antiAlias,
                  child: Stack(
                    children: <Widget>[
                      Image.file(
                        uploadModel.imageFile,
                        width: 300,
                        height: 300,
                      ),
                    ],
                  ),
                );
              } else {
                return Text('');
              }
            }),
          ),
          // Row(
          //   children: [
          //     ...images.map((e) => Image.file(
          //           images[0],
          //           width: 300,
          //           height: 300,
          //         ))
          //   ],
          // ),
          Row(
            children: <Widget>[
              Text(
                "Name: ",
                style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
              ),
              Text(name, style: TextStyle(fontSize: 16)),
            ],
          ),
          SizedBox(height: 20),
          Row(
            children: <Widget>[
              Text(
                "Email: ",
                style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
              ),
              Text(email, style: TextStyle(fontSize: 16)),
            ],
          ),
          SizedBox(height: 20),
          Row(
            children: <Widget>[
              Text(
                "Mobile No: ",
                style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
              ),
              Text(phoneNumber, style: TextStyle(fontSize: 16)),
            ],
          ),

          SizedBox(height: 20),
          Row(
            children: <Widget>[
              Text(
                "Notes: ",
                style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
              ),
              Text(notes, style: TextStyle(fontSize: 16)),
            ],
          ),
          SizedBox(height: 20),
          Row(
            children: <Widget>[
              Text(
                "Address: ",
                style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
              ),
              Text(address, style: TextStyle(fontSize: 16)),
            ],
          ),

          SizedBox(height: 20),
          Row(
            children: <Widget>[
              Text(
                "Age",
                style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
              ),
              Text(age, style: TextStyle(fontSize: 16)),
            ],
          ),
          SizedBox(height: 20),
          Row(
            children: <Widget>[
              Text(
                "Gender: ",
                style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
              ),
              Text(gender, style: TextStyle(fontSize: 16)),
            ],
          ),
          SizedBox(
            height: 20,
          ),
          Center(
            child: FlatButton(
              onPressed: () => widget.onContinue(),
              child: Text('Submit'),
              textColor: Colors.white,
              color: Colors.blue,
            ),
          ),
          SizedBox(
            height: 20,
          ),
        ],
      ),
    );
  }
}
