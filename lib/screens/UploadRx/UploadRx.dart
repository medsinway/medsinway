import 'dart:collection';
import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:medsinway/components/AppBarsWrapper/AppBarBottomBarWrapper.dart';
import 'package:medsinway/components/Dialog/SucessDilog.dart';
import 'package:medsinway/components/Loaders/LoaderDefalult.dart';
import 'package:medsinway/components/Snackbar/SnackbarPage.dart';
import 'package:medsinway/constants.dart';
import 'package:medsinway/helper/Api.dart';
import 'package:medsinway/screens/Home/Home.dart';
import 'package:medsinway/screens/Orders/Orders.dart';
import 'package:medsinway/screens/UploadRx/components/UploadMultipleImages.dart';

import 'Contact.dart';
import 'Personal.dart';
import 'Upload.dart';

class UploadRx extends StatefulWidget {
  UploadRx({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _UploadRxState createState() => _UploadRxState();
}

class _UploadRxState extends State<UploadRx> {
  var currentStep = 0;
  bool isLoading = false;
  bool isShowSnack = false;
  void initState() {
    super.initState();
    // Future.delayed(Duration(seconds: 1)).then((_) => CustomeSnackbar(context));
  }

// Display Snackbar
//   void _displaySnackbar() {
//     Get.snackbar('Hi', 'i am a modern snackbar');
//     //BuildContext con = context;
//     //final snackBar = SnackBar(content: Text(message));
//     //Scaffold.of(con).showSnackBar(snackBar);
//     // Scaffold.of(con).showSnackBar(SnackBar(
//     //     duration: Duration(minutes: 1),
//     //     content: Text('Your snackbar message')));
//   }

  @override
  Widget build(BuildContext context) {
    List<Object> images = MultipleImageUploadState.images;
    var mapData = HashMap<String, String>();
    mapData["name"] = ContactState.controllerName.text;
    mapData["last_name"] = PersonalState.controllerLastName.text;
    mapData["age"] = ContactState.controllerAge.text;
    mapData["gender"] = ContactState.controllerGender.text;
    mapData["email"] = ContactState.controllerEmail.text;
    mapData["address"] = ContactState.controllerAddress.text;
    mapData["phone_number"] = ContactState.controllerPhoneNumber.text;
    mapData["note"] = ContactState.controllerNotes.text;

    void handleOrderSubmit() async {
      setState(() {
        isLoading = true;
      });
      print(mapData);
      //ImageUploadModel uploadModel = images[0];
      // print(uploadModel.imageFile);
      ImageUploadModel imageUpload = new ImageUploadModel();
      getImage(img) {
        print('img');
        //print(img['imageFile']);
        ImageUploadModel uploadModel = img;

        // imageUpload.imageFile = File(img['imageFile']);
        return uploadModel;
      }

      images.removeLast();

      for (var file in images)
        print({
          await MultipartFile.fromFile(getImage(file).imageFile.path,
              filename: getImage(file).imageFile.path.split('/').last)
        }.toList());

      //var imagePoped = [...images];

      FormData formData = FormData.fromMap({
        // "order_image": [
        //   for (var file in images)
        //     ...{
        //       await MultipartFile.fromFile(getImage(file).imageFile.path,
        //           filename: getImage(file).imageFile.path.split('/').last)
        //     }.toList()
        // ],
        "name": mapData["name"],
        "age": mapData["age"],
        "gender": mapData["gender"],
        'phone_number': mapData["phone_number"],
        "email": mapData["email"],
        'address': mapData["address"],
      });
      formData.files.addAll([
        for (var file in images)
          ...{
            MapEntry(
                "order_image",
                await MultipartFile.fromFile(getImage(file).imageFile.path,
                    filename: getImage(file).imageFile.path.split('/').last))
          }.toList()
      ]);

      print('formData');
      //print(jsonDecode(jsonEncode(formData)));
      // Api().tokenProvide();
      var token = await Api().tokenProvide();

      print(token);
      // images.forEach((element) {
      //   formData.a
      // });
      Dio dio = new Dio(BaseOptions(
        headers: {
          'Access-Control-Allow-Origin': '*',
          "acc": "Bearer ${jsonDecode(token)}",
          "accessToken": jsonDecode(token),
          'Content-Type': 'multipart/form-data',
          'Accept-Charset': 'UTF-8'
        },
      ));

      Response response = await dio.post("${Api().Url}/order", data: formData);
      if (response != null) {
        print(response);
        setState(() {
          isLoading = false;
        });
        sucessDilog(
          context,
          msg: kOrderSucessMsg,
          leftBtnTxt: kMyOrdersTxt,
          leftBtnRoute: Orders.routeName,
          rightBtnRoute: Home.routeName,
          rightBtnTxt: kHomeTxt,
        );
      } else {
        print('response');
        print(response);
      }
    }

    void onContinue() {
      setState(() {
        isShowSnack = false;
      });
      setState(() {
        if (currentStep < 3) {
          print(currentStep);

          if (currentStep == 0 && images.length > 1) {
            currentStep = currentStep + 1;
          } else if (currentStep == 0) {
            setState(() {
              isShowSnack = true;
            });

            //CustomeSnackbar(context, title: kUploadPrescriptionSnack);
          }
          if (currentStep == 1 &&
              ContactState.formKey != null &&
              ContactState.formKey.currentState != null &&
              ContactState.formKey.currentState.validate()) {
            currentStep = currentStep + 1;
          } else if (currentStep == 2) {
            handleOrderSubmit();
          }
        } else {
          currentStep = 0;
        }
      });
    }

//https://codingwithdhrumil.com/2020/10/flutter-stepper-widget-example.html
    List<Step> steps = [
      // need to provide steps length to onContinue function
      Step(
        title: Text('Personal'),
        content: Personal(onContinue),
        state: currentStep == 0 ? StepState.editing : StepState.indexed,
        isActive: true,
      ),
      Step(
        title: Text('Contact'),
        content: Contact(onContinue),
        state: currentStep == 1 ? StepState.editing : StepState.indexed,
        isActive: true,
      ),
      Step(
        title: Text('Upload'),
        content: Upload(mapData, images, onContinue),
        state: StepState.complete,
        isActive: true,
      ),
    ];

    return AppBarBottomBarWrapper(
      enableGoBack: true,
      child: Container(
        child: isLoading
            ? LoaderDefault()
            : Stepper(
                currentStep: this.currentStep,
                steps: steps,
                type: StepperType.horizontal,
                onStepTapped: (step) {
                  setState(() {
                    // this is for on click of tab
                    // currentStep = step;
                  });
                },
                onStepContinue: () {
                  setState(() {
                    if (currentStep < steps.length - 1) {
                      if (currentStep == 0 && images.length > 1) {
                        currentStep = currentStep + 1;
                      } else if (currentStep == 1 &&
                          ContactState.formKey.currentState.validate()) {
                        currentStep = currentStep + 1;
                      }
                    } else if (currentStep == 2) {
                      handleOrderSubmit();
                    } else {
                      currentStep = 0;
                    }
                  });
                },
                onStepCancel: () {
                  setState(() {
                    if (currentStep > 0) {
                      currentStep = currentStep - 1;
                    } else {
                      currentStep = 0;
                    }
                  });
                },
                controlsBuilder: (BuildContext context,
                        {VoidCallback onStepContinue,
                        VoidCallback onStepCancel}) =>
                    Container(
                  child: SnackbarPage(isShowSnack, kUploadPrescriptionSnack),
                ),
              ),
      ),
    );
  }
}
