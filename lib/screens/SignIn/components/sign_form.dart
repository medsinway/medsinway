import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:medsinway/components/Loaders/LoaderDefalult.dart';
import 'package:medsinway/helper/Api.dart';
import 'package:medsinway/helper/user.dart';
import 'package:medsinway/screens/Home/Home.dart';

import '../../../components/Buttons/DefaultButton.dart';
import '../../../components/Forms/FormError.dart';
import '../../../constants.dart';
import '../../../size_config.dart';
import '../../ForgotPassword/forgot_password_screen.dart';

class SignForm extends StatefulWidget {
  @override
  _SignFormState createState() => _SignFormState();
}

class _SignFormState extends State<SignForm> {
  final _formKey = GlobalKey<FormState>();
  bool isLoading = false;
  String email;
  String password;
  bool remember = false;
  String signUpError = '';
  final List<String> errors = [];
  String token;
  bool isLoggedIn = false;
  bool isdilogOpen = false;

  void addError({String error}) {
    if (isdilogOpen == false) {
      _showMyDialog();
      setState(() {
        isdilogOpen = true;
      });
    }

    // if (!errors.contains(error))
    //   setState(() {
    //     errors.add(error);
    //   });
  }

  void removeError({String error}) {
    if (errors.contains(error))
      setState(() {
        errors.remove(error);
      });
  }

  void authenticate() async {
    setState(() {
      //isLoading = true;
    });
    var data = {
      "emailOrPhoneNumber": email,
      "password": password,
    };
    http.Response auth =
        await Api().postData(data: json.encode(data), apiUrl: '/login');
    print('json.decode(auth.body)');
    print(auth.body);
    Map<String, dynamic> respData = json.decode(auth.body);

    print(respData);
    if (auth.statusCode == 200 && respData['user']['password'] != null) {
      User().logIn(json.decode(auth.body));
      print('auth.statusCode');
      print(json.decode(auth.body));
      setState(() {
        isLoggedIn = true;
        Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(builder: (BuildContext context) => Home()),
            ModalRoute.withName('/home'));
        // Navigator.pushNamed(context, Home.routeName);
      });
    } else if (auth.statusCode == 401 ||
        respData['message'] == 'User not verified') {
      Navigator.pushNamed(context, '/not_verified_user');
    }
    print(auth.statusCode);
    if (auth.statusCode == 400 ||
        auth.statusCode == 401 ||
        auth.statusCode == 422) {
      setState(() {
        isLoading = false;
        signUpError = 'Entered detailes are wrong';
      });
    }

    // print(json.decode(auth.body));
    // json.decode(res.body);
    // String myurl = "http://trymysolution.com/api/auth/login";
    // http.post(myurl, headers: {
    //   'Accept': 'application/json',
    //   'authorization': 'pass your key(optional)'
    // }, body: {
    //   "email": email,
    //   "password": password,
    // }).then((response) async {
    //   print(response.statusCode);
    //   var body = json.decode(response.body);
    //   SharedPreferences localStorage = await SharedPreferences.getInstance();
    //   localStorage.setString('token', json.encode(body['access_token']));
    //   // localStorage.setString('user', json.encode(body['user']));
    //   //token = jsonDecode(body['access_token'])['token'];
    //   //print(token);
    //   print(body['user']);
    //   print(response.body);
    // });
  }

  Future<void> _showMyDialog() async {
    return showDialog<void>(
      context: context,
      barrierDismissible:
          true, //this means the user must tap a button to exit the Alert Dialog
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Please enter valid credentials'),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                // ExpressOrder(_image),
              ],
            ),
          ),
          actions: <Widget>[
            // FlatButton(
            //   child: Text('My Orders'),
            //   onPressed: () {
            //     Navigator.pushNamed(context, '/my_orders');
            //   },
            // ),
            FlatButton(
              child: Text('Close'),
              onPressed: () {
                isdilogOpen = false;
                Navigator.of(context).pop(true);
              },
            ),
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return isLoading
        ? Container(
            padding: EdgeInsets.symmetric(vertical: 60),
            child: LoaderDefault(),
          )
        : Form(
            key: _formKey,
            child: Column(
              children: [
                buildEmailFormField(),
                SizedBox(height: getProportionateScreenHeightCtx(30, context)),
                buildPasswordFormField(),
                SizedBox(height: getProportionateScreenHeightCtx(30, context)),
                Row(
                  children: [
                    // Checkbox(
                    //   value: remember,
                    //   activeColor: kPrimaryColor,
                    //   onChanged: (value) {
                    //     setState(() {
                    //       remember = value;
                    //     });
                    //   },
                    // ),
                    // Text("Remember me"),
                    Spacer(),
                    GestureDetector(
                      onTap: () => Navigator.pushNamed(
                          context, ForgotPasswordScreen.routeName),
                      child: Text(
                        "Forgot Password",
                        style: TextStyle(decoration: TextDecoration.underline),
                      ),
                    )
                  ],
                ),
                FormError(errors: errors),
                SizedBox(height: getProportionateScreenHeightCtx(40, context)),
                Text(
                  signUpError != '' ? signUpError : '',
                  style: TextStyle(
                    color: Colors.red,
                    fontWeight: FontWeight.bold,
                    fontSize: 16,
                  ),
                ),
                SizedBox(height: getProportionateScreenHeightCtx(40, context)),
                DefaultButton(
                  text: "Continue",
                  press: () {
                    if (_formKey.currentState.validate()) {
                      _formKey.currentState.save();
                      authenticate();

                      // if all are valid then go to success screen
                      // KeyboardUtil.hideKeyboard(context);
                      //
                    }
                  },
                ),
              ],
            ),
          );
  }

  TextFormField buildPasswordFormField() {
    return TextFormField(
      obscureText: true,
      onSaved: (newValue) => password = newValue,
      onChanged: (value) {
        if (value.isNotEmpty) {
          removeError(error: kPassNullError);
        } else if (value.length >= 8) {
          removeError(error: kShortPassError);
        }
        return null;
      },
      validator: (value) {
        if (value.isEmpty) {
          addError(error: kPassNullError);
          return "";
        } else if (value.length < 5) {
          addError(error: kShortPassError);
          return "";
        }
        return null;
      },
      decoration: InputDecoration(
          labelText: "",
          hintText: "Enter your password",
          // If  you are using latest version of flutter then lable text and hint text shown like this
          // if you r using flutter less then 1.20.* then maybe this is not working properly
          floatingLabelBehavior: FloatingLabelBehavior.always,
          suffixIcon: Icon(
              Icons.lock) //CustomSurffixIcon(svgIcon: "assets/icons/Lock.svg"),
          ),
    );
  }

  TextFormField buildEmailFormField() {
    return TextFormField(
      keyboardType: TextInputType.text,
      onSaved: (newValue) => email = newValue,
      onChanged: (value) {
        if (value.isNotEmpty) {
          removeError(error: kEmailNullError);
        } else if (emailValidatorRegExp.hasMatch(value) ||
            phoneNumberValidate.hasMatch(value)) {
          removeError(error: kInvalidEmailError);
        }
        return null;
      },
      validator: (value) {
        if (value.isEmpty) {
          addError(error: kEmailNullError);
          return "";
        }
        // else if (!emailValidatorRegExp.hasMatch(value) ||
        //     !phoneNumberValidate.hasMatch(value)) {
        //   addError(error: kInvalidEmailError);
        //   return "";
        // }
        return null;
      },
      decoration: InputDecoration(
          labelText: "",
          hintText: "Email or Phone Number",
          // If  you are using latest version of flutter then lable text and hint text shown like this
          // if you r using flutter less then 1.20.* then maybe this is not working properly
          floatingLabelBehavior: FloatingLabelBehavior.always,
          suffixIcon: Icon(Icons
              .email_outlined) //CustomSurffixIcon(svgIcon: "assets/icons/Mail.svg"),
          ),
    );
  }
}
