import 'package:flutter/material.dart';
import 'package:medsinway/constants.dart';

import '../../../components/Cards/NoAccountText.dart';
import '../../../size_config.dart';
import 'sign_form.dart';

class Body extends StatelessWidget {
  final String hideBackBtn;
  Body(this.hideBackBtn);
  @override
  Widget build(BuildContext context) {
    Dimensions(context);
    return SafeArea(
      child: SizedBox(
        width: double.infinity,
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: Dimensions.boxWidth * 5),
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(height: Dimensions.boxHeight * 0.4),
                IconButton(
                  padding: EdgeInsets.only(left: 0),
                  alignment: Alignment.centerLeft,
                  icon: Icon(hideBackBtn == null ? Icons.west : null),
                  onPressed: () => Navigator.of(context).pop(),
                ),
                // Stack(
                //   alignment: Alignment.centerLeft,
                //   children: <Widget>[
                //     IconButton(
                //       padding: EdgeInsets.only(left: 0),
                //       alignment: Alignment.centerLeft,
                //       icon: Icon(hideBackBtn == null ? Icons.west : null),
                //       onPressed: () => Navigator.of(context).pop(),
                //     ),
                //     Row(
                //       mainAxisAlignment: MainAxisAlignment.center,
                //       children: <Widget>[
                //         Text('Sign In'),
                //       ],
                //     ),
                //   ],
                // ),
                //SizedBox(height: Dimensions.boxHeight * 7),

                // Text(
                //   "Welcome Back",
                //   style: TextStyle(
                //     color: Colors.black,
                //     fontSize: getProportionateScreenWidth(28),
                //     fontWeight: FontWeight.bold,
                //   ),
                // ),
                // Center(
                //   child: Image.asset(
                //     "assets/images/splash_logo.png",
                //     width: MediaQuery.of(context).size.width / 5,
                //   ),
                // ),
                SizedBox(height: Dimensions.boxHeight * 0.3),
                Center(child: Text("Sign in", style: headingStyle)),
                SizedBox(height: Dimensions.boxHeight * 0.3),
                Center(
                  child: Text(
                    "with your Medsinway credentials",
                    textAlign: TextAlign.center,
                  ),
                ),

                SignForm(),
                SizedBox(height: Dimensions.boxHeight * 0.08),
                // Row(
                //   mainAxisAlignment: MainAxisAlignment.center,
                //   children: [
                //     SocalCard(
                //       icon: "assets/icons/google-icon.svg",
                //       press: () {},
                //     ),
                //     SocalCard(
                //       icon: "assets/icons/facebook-2.svg",
                //       press: () {},
                //     ),
                //     SocalCard(
                //       icon: "assets/icons/twitter.svg",
                //       press: () {},
                //     ),
                //   ],
                // ),
                SizedBox(
                    height: getProportionateScreenHeightCtx(20.0, context)),
                NoAccountText(),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
