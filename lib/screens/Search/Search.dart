import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:http/http.dart' as http;
import 'package:medsinway/constants.dart';
import 'package:medsinway/main.dart';
import 'package:medsinway/screens/Search/BuildSearchBox.dart';
import 'package:medsinway/screens/Search/BuildSearchResults.dart';

final String url = 'https://jsonplaceholder.typicode.com/users';

class UserDetails {
  final int id;
  int count;
  final String firstName, lastName, profileUrl;

  UserDetails(
      {this.id,
      this.count,
      this.firstName,
      this.lastName,
      this.profileUrl =
          'https://i.amz.mshcdn.com/3NbrfEiECotKyhcUhgPJHbrL7zM=/950x534/filters:quality(90)/2014%2F06%2F02%2Fc0%2Fzuckheadsho.a33d0.jpg'});

  factory UserDetails.fromJson(Map<String, dynamic> json) {
    return new UserDetails(
      id: json['id'],
      count: json['count'],
      firstName: json['name'],
      lastName: json['username'],
    );
  }
}

class Search extends StatefulWidget {
  Search({Key key}) : super(key: key);

  @override
  _SearchState createState() => new _SearchState();
}

class _SearchState extends State<Search> {
  List<UserDetails> _searchResult = [];
  List<UserDetails> _userDetails = [];
  bool isShowSnack = false;
  TextEditingController controller = new TextEditingController();

  // Get json result and convert it to model. Then add
  Future<Null> getUserDetails() async {
    final response = await http.get(url);
    final responseJson = json.decode(response.body);

    setState(() {
      for (Map user in responseJson) {
        _userDetails.add(UserDetails.fromJson({...user, 'count': 0}));
      }
    });
  }

  handleAddToCart(count) {
    print(count);
    if (count > 0) {
      StoreProvider.of<AppState>(context).dispatch(AppActions.Increment);
    } else {
      StoreProvider.of<AppState>(context).dispatch(AppActions.IncrementError);
    }
  }

  @override
  void initState() {
    super.initState();

    getUserDetails();
  }

  void handleCount(i, plus) {
    setState(() {
      if (plus && kMaxItemCount > _searchResult[i].count) {
        _searchResult[i].count++;
      } else if (_searchResult[i].count != 0 && !plus) {
        _searchResult[i].count--;
      }
    });

    print(_searchResult[i].count);
  }

  Widget _buildUsersList() {
    return new ListView.builder(
      itemCount: _userDetails.length,
      itemBuilder: (context, index) {
        return new Card(
          child: new ListTile(
            leading: new CircleAvatar(
              backgroundImage: new NetworkImage(
                _userDetails[index].profileUrl,
              ),
            ),
            title: new Text(_userDetails[index].firstName +
                ' ' +
                _userDetails[index].lastName),
          ),
          margin: const EdgeInsets.all(0.0),
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        children: <Widget>[
          StoreConnector<AppState, AppState>(
            converter: (store) => store.state,
            builder: (_, state) {
              return Builder(
                builder: (context) => Container(
                  padding: EdgeInsets.only(bottom: 8),
                  child: Text(
                    '${state.error != '' ? state.error : ''}',
                    style: TextStyle(
                        fontSize: 14,
                        fontWeight: FontWeight.bold,
                        color: kPrimaryColor),
                  ),
                ),
              );
              // Text(
              //         'Cart ${state.counter}',
              //         style: TextStyle(
              //           color: Colors.white,
              //         ),
              //       )
            },
          ),
          Container(
            color: kAppColor,
            //Theme.of(context).primaryColor,
            child: BuildSearchBox(controller, onSearchTextChanged),
          ),
          Expanded(
              child: _searchResult.length != 0 || controller.text.isNotEmpty
                  ? BuildSearchResults(
                      _searchResult, handleCount, handleAddToCart)
                  : Text('')
              //_buildUsersList()
              ),
        ],
      ),
    );
  }

  onSearchTextChanged(String text) async {
    _searchResult.clear();
    if (text.isEmpty) {
      setState(() {});
      return;
    }

    _userDetails.forEach((userDetail) {
      if (userDetail.firstName.contains(text) ||
          userDetail.lastName.contains(text)) _searchResult.add(userDetail);
    });

    setState(() {});
  }
}
