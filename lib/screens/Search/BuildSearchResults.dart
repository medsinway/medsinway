import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:medsinway/constants.dart';
import 'package:medsinway/main.dart';

Widget BuildSearchResults(searchResult, handleCount, handleAddToCart) {
  return new ListView.builder(
    itemCount: searchResult.length,
    itemBuilder: (context, i) {
      return new Card(
        child: Container(
          padding: EdgeInsets.all(5),
          decoration: BoxDecoration(
            border: Border(
              bottom: BorderSide(
                width: 1.0,
                color: kAppColor,
              ),
            ),
          ),
          child: ListTile(
            leading: CircleAvatar(
              backgroundImage: NetworkImage(
                searchResult[i].profileUrl,
              ),
            ),
            title: Row(
              children: [
                Expanded(
                  child: Column(
                    children: [
                      Text(searchResult[i].firstName),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          FlatButton(
                            child: Text(
                              '-',
                              style: TextStyle(fontSize: 30),
                            ),
                            onPressed: () {
                              handleCount(i, false);
                            },
                          ),
                          Text(searchResult[i].count.toString()),
                          FlatButton(
                            child: Text(
                              '+',
                              style: TextStyle(fontSize: 20),
                            ),
                            onPressed: () => handleCount(i, true),
                          ),
                        ],
                      )
                    ],
                  ),
                ),
                StoreConnector<AppState, AppState>(
                  converter: (store) => store.state,
                  builder: (_, state) {
                    return RaisedButton(
                      color: kPrimaryColor,
                      onPressed: () {
                        handleAddToCart(searchResult[i].count);
                      },
                      child: Text(
                        'Add To Cart',
                        style: TextStyle(
                          fontSize: 12,
                          color: Colors.white,
                        ),
                      ),
                    );
                  },
                ),
                // Container(
                //   child: RaisedButton(
                //     color: kPrimaryColor,
                //     onPressed: () => {print('add cart')},
                //     child: Text(
                //       'Add To Cart',
                //       style: TextStyle(
                //         fontSize: 12,
                //         color: Colors.white,
                //       ),
                //     ),
                //   ),
                // ),
              ],
            ),
          ),
        ),
        margin: const EdgeInsets.all(5.0),
      );
    },
  );
}
