import 'package:flutter/material.dart';

Widget BuildSearchBox(controller, onSearchTextChanged) {
  return new Padding(
    padding: const EdgeInsets.all(8.0),
    child: new Card(
      child: new ListTile(
        leading: new Icon(Icons.search),
        title: new TextField(
          controller: controller,
          decoration:
              new InputDecoration(hintText: 'Search', border: InputBorder.none),
          onChanged: onSearchTextChanged,
        ),
        trailing: new IconButton(
          icon: new Icon(Icons.cancel),
          onPressed: () {
            controller.clear();
            onSearchTextChanged('');
          },
        ),
      ),
    ),
  );
}
