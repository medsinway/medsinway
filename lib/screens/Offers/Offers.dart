import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:medsinway/components/Loaders/LoaderDefalult.dart';
import 'package:medsinway/constants.dart';
import 'package:medsinway/helper/Api.dart';
import 'package:medsinway/model/bannersModel.dart';

import '../../components/AppBarsWrapper/AppBarBottomBarWrapper.dart';
import '../../helper/mapIndexed.dart';

class Offers extends StatefulWidget {
  static String routeName = "/offers";
  @override
  _OffersState createState() => _OffersState();
}

class _OffersState extends State<Offers> {
  static String routeName = "/offers";
  String id = '0909';
  dynamic offerData;
  List<BannersModel> offerList;
  // List<OrdersModel> orderList;
  void initState() {
    getOffers();
    super.initState();
  }

  void getOffers() async {
    http.Response offers =
        await Api().getData('/banners', headerz: khomeOffersPage);
    print('offers');
    //print(offers.body);
    // Map ordr = jsonDecode(orders.body);
    // print('orders.body');
    // print(orders.statusCode);
    if (offers.statusCode == 200) {
      var data = json.decode(offers.body);
      var rest = data["data"] as List;

      await setState(() {
        offerData = data["data"];
      });

      print('rest');
      print(rest);

      setState(() {
        offerList = rest
            .map<BannersModel>((json) => BannersModel.fromJson(json))
            .toList();
        //print(orderList[1]);
      });
      print(offerList);
    }
  }

  Map getOffer(offer) {
    Map offerIndexed = jsonDecode(jsonEncode(offer));
    return offerIndexed;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: AppBarBottomBarWrapper(
        enableGoBack: true,
        backRoute: routeName,
        currentRoute: 'offers',
        child: offerList != null && offerList.length > 0
            ? SingleChildScrollView(
                scrollDirection: Axis.vertical,
                child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      ...offerList.mapIndexed(
                        (offer, i) => InkWell(
                          onTap: () => Navigator.pushNamed(
                              context, '/offer_details',
                              arguments: offerData[i]),
                          child: Container(
                            child: Column(
                              children: [
                                Expanded(
                                  child: Container(
                                    width: MediaQuery.of(context).size.width,
                                    child: Image.network(
                                      Api().AssetUrl + offer.bannerImage,
                                      fit: BoxFit.cover,
                                    ),
                                  ),
                                ),
                                SizedBox(
                                  height: 10,
                                ),
                                Text(
                                  offer.bannerHeading != null
                                      ? offer.bannerHeading
                                      : '',
                                  style: TextStyle(fontSize: 16),
                                ),
                              ],
                            ),
                            width: MediaQuery.of(context).size.width,
                            height: 200,
                            //margin: EdgeInsets.only(bottom: 20),
                            padding: EdgeInsets.all(10),
                            decoration: BoxDecoration(
                              border: Border(
                                bottom: BorderSide(
                                  //                   <--- left side
                                  color: Colors.black,
                                  width: 1.0,
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),

                      //provide all the things u want to horizontally scroll here
                    ]),
              )
            : LoaderDefault(),
      ),
    );
  }
}
