import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:medsinway/components/AppBarsWrapper/AppBarBottomBarWrapper.dart';
import 'package:medsinway/components/Buttons/DefaultButton.dart';
import 'package:medsinway/components/Dialog/SucessDilog.dart';
import 'package:medsinway/components/Forms/InputTextField.dart';
import 'package:medsinway/components/Loaders/LoaderDefalult.dart';
import 'package:medsinway/constants.dart';
import 'package:medsinway/helper/Api.dart';
import 'package:medsinway/screens/Offers/Offers.dart';
import 'package:medsinway/screens/UploadRx/components/UploadMultipleImages.dart';

class OfferUpload extends StatefulWidget {
  static String routeName = "/offers_upload";
  @override
  _OfferUploadState createState() => _OfferUploadState();
}

class _OfferUploadState extends State<OfferUpload> {
  static String routeName = "/offers_upload";
  String _currentSelectedItem;
  bool isLoading = false;
  bool isValidImage = true;
  List<Object> images = MultipleImageUploadState.images;
  final _formKey = GlobalKey<FormState>();

  var data = {
    "bannerType": '',
    "bannerHeading": '',
    "bannerDetails": '',
    "bannerTermsAndConditions": '',
  };

  void callBackImage(images) {
    setState(() {
      if (images.length > 1) {
        isValidImage = true;
      } else {
        isValidImage = false;
      }
    });
  }

  onSaved(inputName, value) {
    setState(() {
      data[inputName] = value;
      if (inputName == 'email') {
        data['emailOrPhoneNumber'] = value;
      }
    });
  }

  uploadOffer() async {
    setState(() {
      isLoading = true;
    });
    print(data);
    //ImageUploadModel uploadModel = images[0];
    // print(uploadModel.imageFile);
    ImageUploadModel imageUpload = new ImageUploadModel();
    getImage(img) {
      print('img');
      //print(img['imageFile']);
      ImageUploadModel uploadModel = img;

      // imageUpload.imageFile = File(img['imageFile']);
      return uploadModel;
    }

    images.removeLast();

    for (var file in images)
      print({
        await MultipartFile.fromFile(getImage(file).imageFile.path,
            filename: getImage(file).imageFile.path.split('/').last)
      }.toList());

    //var imagePoped = [...images];

    FormData formData = FormData.fromMap({
      // "order_image": [
      //   for (var file in images)
      //     ...{
      //       await MultipartFile.fromFile(getImage(file).imageFile.path,
      //           filename: getImage(file).imageFile.path.split('/').last)
      //     }.toList()
      // ],

      "bannerType": _currentSelectedItem,
      "bannerHeading": data["bannerHeading"],
      "bannerDetails": data["bannerDetails"],
      'bannerTermsAndConditions': data["bannerTermsAndConditions"],
    });
    formData.files.addAll([
      for (var file in images)
        ...{
          MapEntry(
              "bannerImage",
              await MultipartFile.fromFile(getImage(file).imageFile.path,
                  filename: getImage(file).imageFile.path.split('/').last))
        }.toList()
    ]);

    print('formData');
    //print(jsonDecode(jsonEncode(formData)));
    // Api().tokenProvide();
    var token = await Api().tokenProvide();

    print(token);
    // images.forEach((element) {
    //   formData.a
    // });
    Dio dio = new Dio(BaseOptions(
      headers: {
        'Access-Control-Allow-Origin': '*',
        "acc": "Bearer ${jsonDecode(token)}",
        "accessToken": jsonDecode(token),
        'Content-Type': 'multipart/form-data',
        'Accept-Charset': 'UTF-8'
      },
    ));
    print("${Api().Url}/banners");

    Response response = await dio.post("${Api().Url}/banners", data: formData);
    if (response != null) {
      print(response);
      setState(() {
        isLoading = false;
      });
      sucessDilog(
        context,
        msg: kOfferCreateSucessMsg,
        leftBtnTxt: kOffersPageTxt,
        leftBtnRoute: Offers.routeName,
        rightBtnRoute: kCloseTxt,
        rightBtnTxt: kCloseTxt,
      );
    } else {
      print('response');
      print(response);
    }
  }

  @override
  Widget build(BuildContext context) {
    return AppBarBottomBarWrapper(
      backRoute: routeName,
      currentRoute: 'orderDetails',
      enableGoBack: true,
      child: Container(
        padding: EdgeInsets.all(20),
        child: isLoading
            ? LoaderDefault()
            : SingleChildScrollView(
                child: Form(
                  key: _formKey,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      Center(
                        child: Text(
                          'offer upload',
                          style: TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 20),
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      DropdownButton<String>(
                        items: <String>[
                          'homeMainSlider',
                          'homePromotions',
                          'offersPage',
                          'homeDeals'
                        ].map((String value) {
                          return new DropdownMenuItem<String>(
                            value: value,
                            child: new Text(value),
                          );
                        }).toList(),
                        isExpanded: true,
                        hint: Text("Select Offer Type"),
                        value: _currentSelectedItem,
                        onChanged: (value) {
                          setState(() {
                            _currentSelectedItem = value;
                          });
                        },
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      buildTextNumberFormField(
                        type: 'multiline',
                        placeholder: 'Enter Banner Heading',
                        inputName: 'bannerHeading',
                        onSaved: onSaved,
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      buildTextNumberFormField(
                        type: 'multiline',
                        placeholder: 'Enter Banner Details',
                        inputName: 'bannerDetails',
                        onSaved: onSaved,
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      buildTextNumberFormField(
                        type: 'multiline',
                        placeholder: 'Enter BannerTermsAndConditions',
                        inputName: 'bannerTermsAndConditions',
                        onSaved: onSaved,
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Text('Offer images'),
                      SizedBox(
                        height: 170,
                        width: MediaQuery.of(context).size.width,
                        child: MultipleImageUpload(callBackImage),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      DefaultButton(
                        text: "Continue",
                        press: () {
                          if (_formKey.currentState.validate()) {
                            _formKey.currentState.save();
                            print(data);
                            uploadOffer();
                            // if all are valid then go to success screen
                            // KeyboardUtil.hideKeyboard(context);
                            //
                          }
                        },
                      ),
                    ],
                  ),
                ),
              ),
      ),
    );
  }
}
