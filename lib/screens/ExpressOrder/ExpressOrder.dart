import 'dart:convert';
import 'dart:core';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:medsinway/components/Loaders/LoaderDefalult.dart';
import 'package:medsinway/helper/Api.dart';

import '../../components/AppBarsWrapper/AppBarBottomBarWrapper.dart';
import '../../components/Forms/InputTextField.dart';
import '../../helper/keyboard.dart';

class ExpressOrder extends StatefulWidget {
  File orderImage;
  ExpressOrder(this.orderImage);

  @override
  _ExpressOrderState createState() =>
      _ExpressOrderState(orderImage: this.orderImage);
}

class _ExpressOrderState extends State<ExpressOrder> {
  _ExpressOrderState({this.orderImage});
  File orderImage;
  static String routeName = "/express_order";
  String backRoute;
  String comments;
  bool isLoading = false;
  Map<String, dynamic> data = {'comments': '', 'phoneNumber': ''};
  final _formKey = GlobalKey<FormState>();
  onSaved(input, inputValue) {
    data[input] = inputValue;
  }

  void handleExpressOrder(context) async {
    setState(() {
      isLoading = true;
    });
    String fileName = orderImage.path.split('/').last;
    var token = await Api().tokenProvide();
    FormData formData = FormData.fromMap({
      "order_image": await MultipartFile.fromFile(
        orderImage.path,
        filename: fileName,
      ),
      'phoneNumber': data['phoneNumber'],
      'comments': data['comments'],
      'order_name': 'text2132',
      'pincode': 'text2132',
      'address': 'text2132',
      'note': 'notes',
    });

    Dio dio = new Dio(BaseOptions(
      headers: {
        'Access-Control-Allow-Origin': '*',
        "Authorization": "Bearer ${jsonDecode(token)}",
        'accessToken': jsonDecode(token)
      },
    ));
//"http://trymysolution.com/api/express-order"
    Response response =
        await dio.post('${Api().Url}/express-order', data: formData);

    if (response != null) {
      setState(() {
        isLoading = false;
      });
      _showMyDialog(context);
    }
  }

  Future<void> _showMyDialog(context) async {
    return showDialog<void>(
      context: context,
      barrierDismissible:
          false, //this means the user must tap a button to exit the Alert Dialog
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Your order has been placed'),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                // ExpressOrder(_image),
              ],
            ),
          ),
          actions: <Widget>[
            FlatButton(
              child: Text('My Orders'),
              onPressed: () {
                Navigator.pushNamed(context, '/my_orders');
              },
            ),
            FlatButton(
              child: Text('Home'),
              onPressed: () {
                Navigator.pushNamed(context, '/');
              },
            ),
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return AppBarBottomBarWrapper(
      backRoute: routeName,
      enableGoBack: true,
      child: Container(
        child: isLoading == true
            ? LoaderDefault()
            : SingleChildScrollView(
                child: Card(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      // const ListTile(
                      //   leading: Icon(Icons.album),
                      //   title: Text('The Enchanted Nightingale'),
                      //   subtitle: Text('Music by Julie Gable. Lyrics by Sidney Stein.'),
                      // ),
                      Container(
                        child: Image.file(orderImage, fit: BoxFit.fill),
                        width: 250,
                        height: 250,
                        padding: EdgeInsets.only(
                          top: 20,
                          bottom: 20,
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Form(
                        key: _formKey,
                        child: Column(
                          children: [
                            buildTextNumberFormField(
                              isRequired: false,
                              type: 'phone',
                              placeholder: 'Enter PhoneNumber',
                              label: 'PhoneNumber',
                              inputName: 'phoneNumber',
                              icon: Icon(Icons.phone),
                              onSaved: onSaved,
                            ),
                            buildTextNumberFormField(
                              isRequired: false,
                              type: 'multiline',
                              placeholder:
                                  'do you want to tell somthing to pharamasist ',
                              label: 'Notes',
                              inputName: 'Comments',
                              icon: Icon(Icons.comment),
                              onSaved: onSaved,
                            )
                          ],
                        ),
                      ),

                      Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: <Widget>[
                          TextButton(
                            child: const Text(
                              'Cancel',
                              style: TextStyle(
                                fontSize: 18,
                              ),
                            ),
                            onPressed: () {
                              Navigator.pop(context);
                            },
                          ),
                          const SizedBox(height: 150),
                          TextButton(
                            child: const Text(
                              'Order',
                              style: TextStyle(
                                fontSize: 18,
                              ),
                            ),
                            onPressed: () {
                              if (_formKey.currentState.validate()) {
                                print('_formKey.currentState');
                                print(input);
                                //handleExpressOrder();
                                _formKey.currentState.save();
                                // if all are valid then go to success screen
                                KeyboardUtil.hideKeyboard(context);
                                handleExpressOrder(context);
                                // if(handleExpressOrder() != null){
                                //
                                // }
                                //
                                // Navigator.pushNamed(context, '/my_orders');
                                // Navigator.pushNamed(
                                //     context, LoginSuccessScreen.routeName);
                              }
                              //
                            },
                          ),
                          const SizedBox(width: 8),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
      ),
    );
  }
}
