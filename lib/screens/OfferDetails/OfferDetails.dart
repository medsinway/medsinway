import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:medsinway/components/AppBarsWrapper/AppBarBottomBarWrapper.dart';
import 'package:medsinway/helper/Api.dart';
import 'package:medsinway/model/bannersModel.dart';

class OfferDetails extends StatefulWidget {
  Map offer;
  OfferDetails(this.offer);
  @override
  _OfferDetailsState createState() => _OfferDetailsState();
}

class _OfferDetailsState extends State<OfferDetails> {
  dynamic dataOrder;
  BannersModel offer;
  @override
  void initState() {
    // TODO: implement initState
    //print(widget.offer);
    dataOrder = widget.offer['order'];

    //json.encode(widget.order['order']);
    if (widget.offer != null && json.encode(widget.offer) != null) {
      offer = BannersModel.fromJson(json.decode(json.encode(widget.offer)));
    }
    print('offer');
    print(jsonEncode(offer));

    //setState(() {
    //   offer = jsonDecode(widget.offer);
    // });
    super.initState();
  }

  Text offerHeading(text) {
    return Text(
      text,
      style: TextStyle(
        fontSize: 16,
        fontWeight: FontWeight.bold,
      ),
    );
  }

  String getofferImageUrl() {
    return Api().AssetUrl + widget.offer['bannerImage'];
  }

  @override
  Widget build(BuildContext context) {
    return AppBarBottomBarWrapper(
      enableGoBack: true,
      child: Container(
        padding: EdgeInsets.all(10),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            SizedBox(
              height: 10,
            ),
            Container(
              child: Center(
                child: Text(
                  widget.offer['bannerHeading'],
                  style: TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
            ),
            SizedBox(
              height: 20,
            ),
            Container(
              width: MediaQuery.of(context).size.width / 1.5,
              height: MediaQuery.of(context).size.width / 1.5,
              child: Expanded(child: Image.network(getofferImageUrl())),
            ),
            SizedBox(
              height: 20,
            ),
            offerHeading('Offer Details'),
            SizedBox(
              height: 20,
            ),
            Container(
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Center(
                  child: Text(widget.offer['bannerDetails'] != null
                      ? widget.offer['bannerDetails']
                      : ''),
                ),
              ),
            ),
            offerHeading('Offer Terms and Conditions'),
            Container(
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Center(
                  child: Text(widget.offer['bannerTermsAndConditions'] != null
                      ? widget.offer['bannerTermsAndConditions']
                      : ''),
                ),
              ),
            ),
            SizedBox(
              height: 20,
            ),
          ],
        ),
      ),
    );
  }
}
