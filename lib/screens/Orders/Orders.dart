import 'dart:convert';
import 'dart:core';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:medsinway/components/Loaders/LoaderDefalult.dart';
import 'package:medsinway/helper/Api.dart';
import 'package:medsinway/model/ordersModel.dart';
import 'package:medsinway/screens/Orders/components/OrderHedding.dart';
import 'package:medsinway/screens/Orders/components/OrderImage.dart';
import 'package:medsinway/screens/Orders/orderUtilModals.dart';

import '../../components/AppBarsWrapper/AppBarBottomBarWrapper.dart';
import '../../helper/mapIndexed.dart';

class Orders extends StatefulWidget {
  static String routeName = "/my_orders";
  @override
  _OrdersState createState() => _OrdersState();
}

class _OrdersState extends State<Orders> {
  static String routeName = "/my_orders";
  //OrdersModel order;
  dynamic dataOrder;
  List<OrdersModel> orderList;
  void initState() {
    getOrders();
    super.initState();
  }

  void getOrders() async {
    http.Response orders = await Api().getData('/my_orders');
    // Map ordr = jsonDecode(orders.body);
    print(orders.body);
    // print(orders.statusCode);
    if (orders.statusCode == 200) {
      var data = json.decode(orders.body);
      print('dataOrder');
      print(orders.body);
      var rest = data["data"] as List<dynamic>;
      await setState(() {
        dataOrder = data["data"];
      });
      //var rest2 = data["data"][0];
      // print(rest2);
      setState(() {
        orderList = rest
            .map<OrdersModel>((json) => OrdersModel.fromJson(json))
            .toList();
        //print(orderList[1]);
      });
      print(rest);
    }
  }

  orderImageType(order) {
    var imgList = [];
    if (order.order_image != null && order.order_image.isNotEmpty) {
      imgList = jsonDecode(order.order_image) as List;
      print('order.order_image');
      print(imgList);
    }
    return imgList;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: AppBarBottomBarWrapper(
        enableGoBack: true,
        backRoute: routeName,
        currentRoute: 'orders',
        child: orderList != null && orderList.length > 0
            ? SingleChildScrollView(
                scrollDirection: Axis.vertical,
                child: Column(children: [
                  ...orderList
                      .mapIndexed(
                        (orderItem, i) => Container(
                          child: Row(
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              Container(
                                child: OrderImage(orderImageType(orderItem)),
                                width: 150,
                              ),
                              Container(
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    OrderHedding(orderItem, context),
                                    Text(
                                        'Status: ${OrderStatusModal(orderItem).getStatus()}'),
                                    Text(
                                      '${orderItem.selling_price != null ? 'Price: ${orderItem.selling_price}' : 'Price: Proccesing order'}',
                                    ),
                                    // Text(
                                    //   'Offer applied: ${orderItem.offer_percent != null ? orderItem.offer_percent : 'Proccesing order'}',
                                    // ),
                                    ButtonBar(
                                      alignment: MainAxisAlignment.start,
                                      children: [
                                        FlatButton(
                                          textColor: const Color(0xFF6200EE),
                                          onPressed: () {
                                            // Perform some action
                                          },
                                          child: const Text('Re Order'),
                                        ),
                                        FlatButton(
                                          textColor: const Color(0xFF6200EE),
                                          onPressed: () {
                                            Navigator.pushNamed(
                                                context, '/order_details',
                                                arguments: {
                                                  'order': dataOrder[i]
                                                });

                                            // Navigator.pushNamed(
                                            //     context, '/order_details');
                                            // Perform some action
                                          },
                                          child: const Text('Details '),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                                padding: EdgeInsets.all(10),
                              ),
                            ],
                          ),
                          width: MediaQuery.of(context).size.width,
                          height: 200,
                          padding: EdgeInsets.all(10),
                          decoration: BoxDecoration(
                            border: Border(
                              bottom: BorderSide(
                                //                   <--- left side
                                color: Colors.black,
                                width: 1.0,
                              ),
                            ),
                          ),
                        ),
                      )
                      .toList(),

                  //provide all the things u want to horizontally scroll here
                ]),
              )
            : LoaderDefault(
                text: orderList != null && orderList.length >= 0
                    ? 'No Data Found'
                    : 'Loading ....'),
      ),
    );
  }
}
