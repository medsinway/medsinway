import 'package:flutter/material.dart';
import 'package:medsinway/helper/Api.dart';

Container OrderImage(image) {
  print('image');
  print(image);
  return Container(
    decoration: BoxDecoration(
      borderRadius: BorderRadius.circular(10),
      border: Border.all(
        width: 1,
        color: Colors.blueGrey,
      ),
    ),
    child: image != null
        ? SingleChildScrollView(
            scrollDirection: Axis.horizontal,
            child: Row(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                ...image.map((img) => Container(
                      width: image.length > 1 ? 120 : 150,
                      padding: const EdgeInsets.all(5.0),
                      margin: const EdgeInsets.all(5.0),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(5),
                        border: Border.all(
                          width: 1,
                          color: image.length > 1 ? Colors.grey : Colors.white,
                        ),
                      ),
                      child: Image.network(Api().AssetUrl + img),
                    ))
              ],
            ),
          )
        : Align(
            alignment: Alignment
                .center, // Align however you like (i.e .centerRight, centerLeft)
            child: Text("no image"),
          ),
  );
}
