import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:medsinway/screens/Orders/orderUtilModals.dart';

Container OrderHedding(order, context) {
  print('DateTime.parse(order.created_at).isUtc');
  var formatter = new DateFormat('dd-MM-yyyy');

  print(order.created_at);
  //print(toIST('${DateTime().parse(order.created_at).toLocal()}'));
  String status = OrderStatusModal(order).status;
  String statusDate = OrderStatusModal(order).statusDate;
  return Container(
    width: MediaQuery.of(context).size.width / 2,
    child: Text(
      //'Order on ${DateTime.parse(order.created_at).toLocal()}',
      'Order on ${order.created_at}',
      overflow: TextOverflow.ellipsis,
      maxLines: 2,
      style: TextStyle(
        fontSize: 16,
        fontWeight: FontWeight.bold,
      ),
    ),
  );
}
