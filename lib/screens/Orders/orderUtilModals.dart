class OrderStatusModal {
  var order;
  OrderStatusModal(this.order);
  String status;
  String statusDate;
  String orderedOn;

  getStatus() {
    orderedOn = order.created_at;

    switch (order.order_status) {
      case "outForDelevery":
        status = 'Out for Delivery';
        statusDate = order.updated_at;
        // do something
        break;
      case "Decline":
        status = "Declined";
        statusDate = order.updated_at;
        break;
      case "Accept":
        status = 'Accepted';
        statusDate = order.updated_at;
        break;
      case "Delivered":
        status = 'Delivered';
        statusDate = order.updated_at;
        break;
      case "Pending":
        status = 'Pending';
        statusDate = order.updated_at;
        break;
      default:
        status = 'Ordered';
        statusDate = order.created_at;
        break;
    }
    return status;
  }
}
