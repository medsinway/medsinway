import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:medsinway/helper/Api.dart';
import 'package:medsinway/model/ordersModel.dart';
import 'package:medsinway/screens/OrderDetails/components/OrderProgressBar.dart';

import '../../components/AppBarsWrapper/AppBarBottomBarWrapper.dart';

class OrderDetails extends StatefulWidget {
  final Map order;
  OrderDetails(this.order);
  @override
  _OrderDetailsState createState() => _OrderDetailsState();
}

class _OrderDetailsState extends State<OrderDetails> {
  static String routeName = "/";
  OrdersModel order;
  dynamic dataOrder;
  List orderImages;
  @override
  void initState() {
    setState(() {
      dataOrder = widget.order['order'];
      //json.encode(widget.order['order']);
      if (widget.order['order'] != null &&
          json.encode(widget.order['order']) != null) {
        order = OrdersModel.fromJson(
            json.decode(json.encode(widget.order['order'])));
      }
    });

    // TODO: implement initState
    super.initState();
  }

  orderImageType(order) {
    var imgList = [];
    if (order.order_image != null && order.order_image.isNotEmpty) {
      print('order.order_image.toString().matchAsPrefix(' ')');
      print(order.order_image);
      imgList = jsonDecode(order.order_image);
      // if (order.order_image.toString().indexOf(('[')) > -1) {
      //   imgList = jsonDecode(order.order_image) as List;
      // } else {
      //   imgList = [order.order_image];
      // }
      //
      // print(order.order_image_path);
      //
      // if (order.order_image != null && order.order_image.isNotEmpty) {
      //   var imgUrl = [];
      //   //  print(order.order_image_path + imgList[0]);
      //   imgList.forEach((element) {
      //     imgUrl.add(order.order_image_path + element);
      //   });
      //
      //   return [...imgUrl];
      // } else if (order.prescription_image_path != null &&
      //     order.prescription_image != null &&
      //     order.prescription_image.isNotEmpty) {
      //   var imgUrl = [];
      //   imgList.forEach((element) {
      //     imgUrl.add(order.prescription_image_path + order.prescription_image);
      //   });
      //   return [...imgUrl];
      // }
    }

    return imgList;
  }

  Card OrderImage(images) {
    return Card(
      // shape: RoundedRectangleBorder(
      //   side: new BorderSide(color: Colors.blue, width: 3.0),
      //   borderRadius: BorderRadius.circular(4.0),
      // ),
      child: Row(children: [
        ...images.map((img) => Container(
              decoration: BoxDecoration(
                border: Border.all(color: Colors.black),
              ),
              padding: EdgeInsets.all(10),
              margin: EdgeInsets.all(5),
              child: Center(
                child: Image.network(
                  Api().AssetUrl + img,
                  width: MediaQuery.of(context).size.width / 1.4,
                  height: MediaQuery.of(context).size.height * 0.65,
                ),
              ),
            )),
      ]),
    );
  }

  @override
  Widget build(BuildContext context) {
    // var orderData = json.encode(order);
    // var orderNme = json.decode(orderData)['order'];
    print('order-------');
    // print(order != null ?? order.order_id);
    // print(jsonEncode(order));
    // print(json.decode(orderData) != null
    //     ? json.decode(orderData)['order']
    //     : 'cool');
    // print(OrdersModel.fromJson(orderNme).order_id);
    print('order-------');

    //Map<String, dynamic> user = jsonDecode(order);

    return AppBarBottomBarWrapper(
      backRoute: routeName,
      bgColor: Colors.white70,
      currentRoute: 'orderDetails',
      enableGoBack: true,
      child: Container(
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                child: Text('Order Id -  A676767WWMM'),
                margin: EdgeInsets.only(bottom: 10),
                padding: EdgeInsets.symmetric(vertical: 15, horizontal: 10),
                width: MediaQuery.of(context).size.width,
                decoration: BoxDecoration(
                  color: Colors.white,
                  border: Border(
                    bottom: BorderSide(
                      //                    <--- top side
                      color: Colors.blueGrey,
                      width: 1.0,
                    ),
                  ),
                ),
              ),
              Container(
                width: MediaQuery.of(context).size.width / 0.2,
                //height: MediaQuery.of(context).size.height * 0.8,
                color: Colors.white,
                child: Column(
                  children: [
                    SizedBox(
                      height: 10,
                    ),
                    Text(
                      'Prescription Images',
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    SingleChildScrollView(
                      scrollDirection: Axis.horizontal,
                      child: Container(
                        child: OrderImage(
                          orderImageType(order),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: 10),
                padding: EdgeInsets.all(15),
                width: MediaQuery.of(context).size.width,
                child: Column(
                  children: [
                    SizedBox(
                      height: 10,
                    ),
                    Text(
                      'Estimated Price',
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                      ),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Text(order.selling_price != null
                        ? 'Price: ${order.selling_price}'
                        : 'Price not updated'),
                    Text(order.offer_percent != null
                        ? 'Offer: ${order.selling_price}'
                        : 'Price not updated'),
                    SizedBox(
                      height: 10,
                    )
                    // Text(order.offer_percent != null
                    //     ? order.offer_percent
                    //     : 'No notes provided for this order'),
                    // Text(
                    //   order.notes != null
                    //       ? order.notes
                    //       : 'No notes provided for this order',
                    //   style: TextStyle(fontSize: 16),
                    //),
                  ],
                ),
                decoration: BoxDecoration(
                  color: Colors.white,
                  border: Border(
                    bottom: BorderSide(
                      //                    <--- top side
                      color: Colors.blueGrey,
                      width: 1.0,
                    ),
                  ),
                ),
              ),
              Container(
                width: MediaQuery.of(context).size.width,
                margin: EdgeInsets.only(bottom: 1, top: 10),
                padding: EdgeInsets.only(bottom: 30, top: 10),
                //height: 200,
                color: Colors.white,
                child: Column(
                  children: [
                    SizedBox(
                      height: 10,
                    ),
                    Text(
                      'Address',
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                      ),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Text(order.address != null
                        ? order.address
                        : 'Address not yet updated for this order'),
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.symmetric(vertical: 10),
                padding: EdgeInsets.all(20),
                width: MediaQuery.of(context).size.width,
                child: Column(
                  children: [
                    SizedBox(
                      height: 10,
                    ),
                    Text(
                      'Notes',
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                      ),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Text(
                      order.note != null
                          ? order.note
                          : 'No notes provided for this order',
                      style: TextStyle(fontSize: 16),
                    ),
                  ],
                ),
                decoration: BoxDecoration(
                  color: Colors.white,
                  border: Border(
                    bottom: BorderSide(
                      //                    <--- top side
                      color: Colors.blueGrey,
                      width: 1.0,
                    ),
                  ),
                ),
              ),
              Container(
                color: Colors.white,
                child: Column(
                  children: [
                    SizedBox(
                      height: 10,
                    ),
                    Text(
                      'Order Status',
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    dataOrder != null
                        ? OrderProgressBar({'order': dataOrder})
                        : Text('Loading...')
                  ],
                ),
              ),
              SizedBox(
                height: 200,
                child: Container(
                  color: Colors.white,
                  margin: EdgeInsets.symmetric(vertical: 10),
                  padding: EdgeInsets.all(20),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      Container(
                        margin: EdgeInsets.all(10),
                        child: ElevatedButton(
                          child: Text(
                            "Re Order +",
                            style: TextStyle(
                              fontSize: 16,
                              fontWeight: FontWeight.bold,
                              color: Colors.white,
                            ),
                          ),
                          //color: Color(0xFFEE803A),
                          onPressed: () {},
                          style: ElevatedButton.styleFrom(
                              primary: Color(0xFFEE803A),
                              padding: EdgeInsets.symmetric(
                                  vertical: 10, horizontal: 40)),
                        ),
                      ),
                      SizedBox(
                        width: 10,
                      ),
                      Container(
                        margin: EdgeInsets.all(10),
                        child: ElevatedButton(
                          style: ElevatedButton.styleFrom(
                              primary: Color(0xFF3D8C65),
                              padding: EdgeInsets.symmetric(
                                  vertical: 10, horizontal: 20)),
                          onPressed: () {},
                          child: Text(
                            'Pay Rs.1500/-',
                            style: TextStyle(
                              fontSize: 20,
                              fontWeight: FontWeight.w100,
                              color: Colors.white,
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
