import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:medsinway/model/ordersModel.dart';

class MiniOrderProgressStep extends StatefulWidget {
  final Map order;
  MiniOrderProgressStep(this.order);

  @override
  _MiniOrderProgressStepState createState() => _MiniOrderProgressStepState();
}

class _MiniOrderProgressStepState extends State<MiniOrderProgressStep> {
  int current_step = 0;
  OrdersModel order;
  @override
  void initState() {
    setState(() {
      if (widget.order['order'] != null &&
          json.encode(widget.order['order']) != null) {
        order = OrdersModel.fromJson(
            json.decode(json.encode(widget.order['order'])));
      }
    });

    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    List<Step> steps = [
      Step(
        title: Text('Order Placed'),
        subtitle: Text('on 12/01/2020'),
        content: Center(child: Text('')),
        state: StepState.complete,
        isActive: true,
      ),
      Step(
        title:
            Text(order.order_status != null ? order.order_status : 'Pending'),
        content: Text('World!'),
        isActive: false,
      ),
    ];

    return InkWell(
      onTap: () {
        Navigator.pushNamed(context, '/order_full_status',
            arguments: {'order': widget.order['order']});
      },
      child: Container(
          child: Stepper(
        currentStep: this.current_step,
        steps: steps,
        type: StepperType.vertical,
        // onStepTapped: (step) {
        //   setState(() {
        //     current_step = step;
        //   });
        // },
        controlsBuilder: (BuildContext context,
                {VoidCallback onStepContinue, VoidCallback onStepCancel}) =>
            Container(),

        // onStepTapped: (step) {
        //   setState(() {
        //     current_step = step;
        //   });
        // },
        // onStepContinue: () {
        //   setState(() {
        //     if (current_step < steps.length - 1) {
        //       current_step = current_step + 1;
        //     } else {
        //       current_step = 0;
        //     }
        //   });
        // },
        // onStepCancel: () {
        //   setState(() {
        //     if (current_step > 0) {
        //       current_step = current_step - 1;
        //     } else {
        //       current_step = 0;
        //     }
        //   });
        // },
      )),
    );
  }
}
