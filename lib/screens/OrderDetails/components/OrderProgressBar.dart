import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:medsinway/model/ordersModel.dart';

class OrderProgressBar extends StatefulWidget {
  final Map order;
  OrderProgressBar(this.order);
  @override
  _OrderProgressBarState createState() => _OrderProgressBarState();
}

class _OrderProgressBarState extends State<OrderProgressBar> {
  int current_step = 0;
  OrdersModel order;
  void initState() {
    setState(() {
      if (widget.order['order'] != null &&
          json.encode(widget.order['order']) != null) {
        order = OrdersModel.fromJson(
            json.decode(json.encode(widget.order['order'])));
        print('order---//');
        print(widget.order['order']);
        print('order---//');
      }
    });

    // TODO: implement initState
    super.initState();
  }

  // toIST(date) {
  //   DateTime tempDate =
  //       new DateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").parse(date);
  //
  //   var inputDate = DateTime.parse(tempDate.toString());
  //   var outputFormat = DateFormat('dd/MM/yyyy hh:mm a');
  //   var outputDate = outputFormat.format(inputDate);
  //   return outputDate;
  // }

  orderHedding(orderStatus, createdAt, updatedAt) {
    Map<String, dynamic> orderStatusMap = {
      'isAccepted': false,
      'isDelivered': false,
      'isOutForDelivery': false,
      'isDeclined': false,
      'isPending': false
    };
    String status;
    String statusDate;
    print('cool----');
    print(orderStatus);
    switch (orderStatus) {
      case "outForDelevery":
        status = 'OutForDelivery';
        orderStatusMap['isOutForDelivery'] = true;
        orderStatusMap['isAccepted'] = true;
        statusDate = updatedAt;
        // do something
        break;
      case "Decline":
        status = "Declined";
        orderStatusMap['isDeclined'] = true;
        statusDate = updatedAt;
        break;
      case "Accept":
        status = 'Accepted';
        orderStatusMap['isAccepted'] = true;
        statusDate = updatedAt;
        break;
      case "Delivered":
        orderStatusMap['isDelivered'] = true;
        orderStatusMap['isAccepted'] = true;
        orderStatusMap['isOutForDelivery'] = true;
        status = 'Delivered';
        statusDate = updatedAt;
        break;
      case "Pending":
        orderStatusMap['isPending'] = true;
        status = 'Delivered';
        statusDate = updatedAt;
        break;
      default:
        status = 'Ordered';
        orderStatusMap['isAccepted'] = true;
        statusDate = createdAt;
        break;
    }
    return orderStatusMap;
  }

  List<Step> steps(currentOrder) {
    final orderPlacedAt = currentOrder.created_at;
    print(orderPlacedAt);
    Map stat;
    if (order != null) {
      stat = orderHedding(currentOrder.order_status, currentOrder.created_at,
          currentOrder.created_at);
    }

    return [
      Step(
        title: Text('Ordered'),
        content: orderPlacedAt != null
            ? Center(child: Text('on $orderPlacedAt'))
            : Text('Loading...'),
        state: StepState.complete,
        isActive: true,
      ),
      Step(
        title: Text(stat != null && stat['isAccepted']
            ? 'Accepted'
            : stat != null && stat['isDeclined']
                ? 'Declined'
                : 'Pending'),
        content: Text('World!'),
        isActive: stat['isAccepted'] || stat['isDeclined'],
        state: (stat['isAccepted'] || stat['isPending'])
            ? StepState.complete
            : StepState.error,
      ),
      Step(
        title: Text('Out For Delivery'),
        content: Text('Hello World!'),
        state: stat != null && stat['isOutForDelivery']
            ? StepState.complete
            : StepState.disabled,
        isActive: stat != null && stat['isOutForDelivery'],
      ),
      Step(
        title: Text('Delivered'),
        content: Text('Hello World!r'),
        state: stat != null && stat['isDelivered']
            ? StepState.complete
            : StepState.disabled,
        isActive: stat != null && stat['isDelivered'],
      ),
    ];
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        child: Stepper(
      physics: ClampingScrollPhysics(),
      currentStep: this.current_step,
      steps: steps(order),
      type: StepperType.vertical,
      onStepTapped: (step) {
        setState(() {
          current_step = step;
        });
      },
      controlsBuilder: (BuildContext context,
              {VoidCallback onStepContinue, VoidCallback onStepCancel}) =>
          Container(),

      // onStepTapped: (step) {
      //   setState(() {
      //     current_step = step;
      //   });
      // },
      // onStepContinue: () {
      //   setState(() {
      //     if (current_step < steps.length - 1) {
      //       current_step = current_step + 1;
      //     } else {
      //       current_step = 0;
      //     }
      //   });
      // },
      // onStepCancel: () {
      //   setState(() {
      //     if (current_step > 0) {
      //       current_step = current_step - 1;
      //     } else {
      //       current_step = 0;
      //     }
      //   });
      // },
    ));
  }
}
