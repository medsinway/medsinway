import 'package:flutter/material.dart';

import '../../../constants.dart';
import '../../../size_config.dart';
import 'sign_up_form.dart';

class Body extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: SizedBox(
        width: double.infinity,
        child: Padding(
          padding: EdgeInsets.symmetric(
            horizontal: getProportionateScreenWidthCtx(20, context),
          ),
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SizedBox(height: MediaQuery.of(context).size.height * 0.04),
                // IconButton(
                //   padding: EdgeInsets.only(left: 0),
                //   alignment: Alignment.centerLeft,
                //   icon: Icon(Icons.west),
                //   onPressed: () => Navigator.of(context).pop(),
                // ),
                Stack(
                  alignment: Alignment.topLeft,
                  children: <Widget>[
                    IconButton(
                      padding: EdgeInsets.only(left: 0),
                      alignment: Alignment.centerLeft,
                      icon: Icon(Icons.west),
                      onPressed: () => Navigator.of(context).pop(),
                    ),
                    Column(
                      children: [
                        Center(
                            child:
                                Text("Register Account", style: headingStyle)),
                        Center(
                          child: Text(
                            "With Your Details",
                            textAlign: TextAlign.center,
                          ),
                        ),
                      ],
                    ),

                    // Row(
                    //   mainAxisAlignment: MainAxisAlignment.center,
                    //   children: <Widget>[
                    //     Text('Sign Up'),
                    //   ],
                    // ),
                  ],
                ),
                // 4%

                SizedBox(height: MediaQuery.of(context).size.height * 0.04),
                SignUpForm(),
                SizedBox(height: MediaQuery.of(context).size.height * 0.08),
                // Row(
                //   mainAxisAlignment: MainAxisAlignment.center,
                //   children: [
                //     SocalCard(
                //       icon: "assets/icons/google-icon.svg",
                //       press: () {},
                //     ),
                //     SocalCard(
                //       icon: "assets/icons/facebook-2.svg",
                //       press: () {},
                //     ),
                //     SocalCard(
                //       icon: "assets/icons/twitter.svg",
                //       press: () {},
                //     ),
                //   ],
                // ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
