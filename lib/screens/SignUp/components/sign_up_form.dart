import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:medsinway/components/Forms/InputTextField.dart';
import 'package:medsinway/components/Loaders/LoaderDefalult.dart';
import 'package:medsinway/helper/Api.dart';

import '../../../components/Buttons/DefaultButton.dart';
import '../../../components/Forms/FormError.dart';
import '../../../constants.dart';
import '../../../helper/keyboard.dart';
import '../../../size_config.dart';

class Data {
  String email;
  String password;
  String phone_number;
  String name;
  int role_id;
  Data({this.email, this.phone_number, this.password, this.name, this.role_id});
  getData() {
    return {
      'email': this.email,
      'phone_number': this.phone_number,
      'password': this.password,
      'name': this.name,
      'role_id': this.role_id,
    };
  }
}

class SignUpForm extends StatefulWidget {
  @override
  _SignUpFormState createState() => _SignUpFormState();
}

class _SignUpFormState extends State<SignUpForm> {
  final _formKey = GlobalKey<FormState>();
  bool isLoading = false;
  String signUpError = '';
  bool signUpSucess = false;
  String email;
  String password;
  String conform_password;
  String phone_number;
  bool remember = false;
  final List<String> errors = [];

  void addError({String error}) {
    if (!errors.contains(error))
      setState(() {
        errors.add(error);
      });
  }

  void removeError({String error}) {
    if (errors.contains(error))
      setState(() {
        errors.remove(error);
      });
  }

  var data = {
    "email": '',
    "phoneNumber": '',
    "password": '',
    "name": '',
    'role_id': 1,
    'emailOrPhoneNumber': ''
  };

  Future handleSignUp() async {
    setState(() {
     // isLoading = true;
    });
    http.Response auth =
        await Api().postData(data: json.encode(data), apiUrl: '/signup');
    Map respData = json.decode(auth.body);
    print(auth.statusCode);
    print('auth.statusCode');
    print(respData);
    print(auth.statusCode);
    if (auth.statusCode == 422 || auth.statusCode == 400) {
      setState(() {
        isLoading = false;
        signUpSucess = false;
        signUpError =
            'Looks entered incorrect details or you may already Registered';
      });
    }

    if (auth.statusCode == 200) {
      setState(() {
        signUpSucess = true;
        signUpError == null;
        isLoading = false;
      });
      if (signUpSucess == true && signUpError != null) {
        Navigator.pushNamed(context, '/not_verified_user');
      }
    } else if (auth.statusCode == 401 ||
        respData['message'] == 'User not verified') {
      Navigator.pushNamed(context, '/not_verified_user');
    }
  }

  onSaved(inputName, value) {
    setState(() {
      data[inputName] = value;
      if (inputName == 'email') {
        data['emailOrPhoneNumber'] = value;
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return isLoading
        ? LoaderDefault()
        : Form(
            key: _formKey,
            child: Column(
              children: [
                buildTextNumberFormField(
                    type: 'text',
                    placeholder: 'Enter Your Name',
                    inputName: 'name',
                    onSaved: onSaved),
                buildTextNumberFormField(
                    type: 'phone',
                    placeholder: 'Enter Your Phone Number',
                    inputName: 'phone_number',
                    onSaved: onSaved),
                buildTextNumberFormField(
                    placeholder: 'Enter Your Email',
                    inputName: 'email',
                    type: 'email',
                    onSaved: onSaved),
                // buildEmailFormField(),
                SizedBox(height: getProportionateScreenHeightCtx(30, context)),
                buildPasswordFormField(),
                SizedBox(height: getProportionateScreenHeightCtx(30, context)),
                buildConformPassFormField(),
                FormError(errors: errors),
                SizedBox(height: getProportionateScreenHeightCtx(20, context)),
                Text(
                  signUpError != '' ? signUpError : '',
                  style: TextStyle(
                    color: Colors.red,
                    fontWeight: FontWeight.bold,
                    fontSize: 16,
                  ),
                ),
                SizedBox(
                  child: signUpError != ''
                      ? FlatButton(
                          child: Text(
                            'Login',
                            style: TextStyle(
                              color: Colors.orange,
                              fontWeight: FontWeight.bold,
                              fontSize: 16,
                            ),
                          ),
                          onPressed: () {
                            Navigator.pushNamed(context, '/sign_in');
                          },
                        )
                      : null,
                  height: getProportionateScreenHeightCtx(5, context),
                ),

                Text(
                  kPrivacyPolicy,
                  textAlign: TextAlign.center,
                  style: Theme.of(context).textTheme.caption,
                ),
                SizedBox(height: getProportionateScreenHeightCtx(20, context)),
                DefaultButton(
                  text: "Continue",
                  press: () async {
                    if (_formKey.currentState.validate()) {
                      _formKey.currentState.save();
                      KeyboardUtil.hideKeyboard(context);
                      await handleSignUp();

                      // if all are valid then go to success screen
                      // Navigator.pushNamed(context, CompleteProfileScreen.routeName);
                    }
                  },
                ),
              ],
            ),
          );
  }

  TextFormField buildConformPassFormField() {
    return TextFormField(
      obscureText: true,
      onSaved: (newValue) => conform_password = newValue,
      onChanged: (value) {
        if (value.isNotEmpty) {
          removeError(error: kPassNullError);
        } else if (value.isNotEmpty && data['password'] == conform_password) {
          removeError(error: kMatchPassError);
        }
        conform_password = value;
      },
      validator: (value) {
        if (value.isEmpty) {
          addError(error: kPassNullError);
          return "";
        } else if ((data['password'] != value)) {
          addError(error: kMatchPassError);
          return "";
        }
        return null;
      },
      decoration: InputDecoration(
          hintText: "Re-enter your password",
          // If  you are using latest version of flutter then lable text and hint text shown like this
          // if you r using flutter less then 1.20.* then maybe this is not working properly
          floatingLabelBehavior: FloatingLabelBehavior.always,
          suffixIcon: Icon(
              Icons.lock) //CustomSurffixIcon(svgIcon: "assets/icons/Lock.svg"),
          ),
    );
  }

  TextFormField buildPasswordFormField() {
    return TextFormField(
      obscureText: true,
      onSaved: (newValue) =>
          data['password'] = newValue, //onSaved('password', newValue),
      onChanged: (value) {
        if (value.isNotEmpty) {
          removeError(error: kPassNullError);
        } else if (value.length >= 8) {
          removeError(error: kShortPassError);
        }
        data['password'] = value;
        print(data);
      },
      validator: (value) {
        if (value.isEmpty) {
          addError(error: kPassNullError);
          return "";
        } else if (value.length < 8) {
          addError(error: kShortPassError);
          return "";
        }
        return null;
      },
      decoration: InputDecoration(
        hintText: "Enter your password",
        // If  you are using latest version of flutter then lable text and hint text shown like this
        // if you r using flutter less then 1.20.* then maybe this is not working properly
        floatingLabelBehavior: FloatingLabelBehavior.always,
        suffixIcon: Icon(
            Icons.lock), //CustomSurffixIcon(svgIcon: "assets/icons/Lock.svg"),
      ),
    );
  }

  TextFormField buildEmailFormField() {
    return TextFormField(
      keyboardType: TextInputType.emailAddress,
      onSaved: (newValue) => email = newValue,
      onChanged: (value) {
        if (value.isNotEmpty) {
          removeError(error: kEmailNullError);
        } else if (emailValidatorRegExp.hasMatch(value)) {
          removeError(error: kInvalidEmailError);
        }
        return null;
      },
      validator: (value) {
        if (value.isEmpty) {
          addError(error: kEmailNullError);
          return "";
        } else if (!emailValidatorRegExp.hasMatch(value)) {
          addError(error: kInvalidEmailError);
          return "";
        }
        return null;
      },
      decoration: InputDecoration(
          labelText: "Email",
          hintText: "Enter your email",
          // If  you are using latest version of flutter then lable text and hint text shown like this
          // if you r using flutter less then 1.20.* then maybe this is not working properly
          floatingLabelBehavior: FloatingLabelBehavior.always,
          suffixIcon: Icon(Icons
              .mail_outline) //CustomSurffixIcon(svgIcon: "assets/icons/Mail.svg"),
          ),
    );
  }
}
