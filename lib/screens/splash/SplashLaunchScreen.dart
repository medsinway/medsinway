import 'dart:async';

import 'package:flutter/material.dart';
import 'package:medsinway/constants.dart';
import 'package:medsinway/helper/user.dart';
import 'package:medsinway/screens/Home/Home.dart';
import 'package:medsinway/screens/splash/splash_screen.dart';
import 'package:medsinway/size_config.dart';

class SplashLaunchScreen extends StatefulWidget {
  @override
  _SplashLaunchScreenState createState() => _SplashLaunchScreenState();
}

class _SplashLaunchScreenState extends State<SplashLaunchScreen> {
  bool isLogged = false;
  startTime() {
    return new Timer(Duration(milliseconds: 1500), navigateUser);
  }

  @override
  void initState() {
    super.initState();
    getStoredValue();
  }

  void getStoredValue() async {
    bool prefs = await User().checkIfLoggedIn();
    // prefs.setBool('isLoggin', true);
    if (prefs
        // !prefs.containsKey('isLoggin')
        ) {
      // prefs.setBool('isLoggin', false);
      print("login set to False");
    }

    isLogged = prefs; // .getBool('isLoggin');
    print("Status in splash -----------$isLogged");
    startTime();
  }

  @override
  Widget build(BuildContext context) {
    // SystemChrome.setEnabledSystemUIOverlays([]);
    return Scaffold(
      body: Container(
        height: double.infinity,
        width: double.infinity,
        color: Colors.white,
        // decoration: BoxDecoration(
        //     gradient: LinearGradient(
        //   begin: Alignment.topLeft,
        //   end: Alignment.bottomRight,
        //   colors: <Color>[
        //     Color(0xFFEE803A),
        //     Color(0xFFf06b18),
        //     // colorStyle.splashPink,
        //     // colorStyle.splashOrange,
        //   ],
        // )),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.asset(
              "assets/images/splash_logo.png",
              width: MediaQuery.of(context).size.width / 2.5,
            ),
            Text(
              "MEDSINWAY",
              style: TextStyle(
                fontSize: getProportionateScreenWidthCtx(20, context),
                color: kPrimaryColor,
                fontWeight: FontWeight.bold,
                fontFamily: "Roboto",
              ),
            ),
          ],
        ),
      ),
    );
  }

  void navigateUser() {
    if (isLogged == true) {
      Navigator.pushAndRemoveUntil(
          context,
          MaterialPageRoute(builder: (BuildContext context) => Home()),
          ModalRoute.withName('/home'));
      // Navigator.of(context).pushReplacementNamed('/home');
      // Navigator.of(context).pushReplacement(
      //     PageRouteBuilder(pageBuilder: (_, __, ___) => Home()));
    } else {
      Navigator.pushAndRemoveUntil(
          context,
          MaterialPageRoute(builder: (BuildContext context) => SplashScreen()),
          ModalRoute.withName('/splash'));
      // Navigator.of(context).pushReplacementNamed('/splash');
      // Navigator.of(context).pushReplacement(
      //     PageRouteBuilder(pageBuilder: (_, __, ___) => SplashScreen()));
    }
  }
}
