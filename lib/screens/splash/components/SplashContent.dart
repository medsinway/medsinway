import 'package:flutter/material.dart';

import '../../../constants.dart';
import '../../../size_config.dart';

class SplashContent extends StatelessWidget {
  const SplashContent({
    Key key,
    this.text,
    this.image,
  }) : super(key: key);
  final String text, image;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Spacer(),
        // Center(
        //   child: Image.asset(
        //     "assets/images/splash_logo.png",
        //     width: MediaQuery.of(context).size.width / 7,
        //   ),
        // ),
        Text(
          "MEDSINWAY",
          style: TextStyle(
            fontSize: getProportionateScreenWidth(36),
            color: kPrimaryColor,
            fontWeight: FontWeight.bold,
          ),
        ),
        Text(
          text,
          textAlign: TextAlign.center,
        ),
        Spacer(flex: 2),
        Image.asset(
          image,
          height: getProportionateScreenHeightCtx(265, context),
          width: getProportionateScreenHeightCtx(235, context),
        ),
      ],
    );
  }
}
