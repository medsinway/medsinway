import 'package:flutter/material.dart';
import 'package:medsinway/components/AppBarsWrapper/AppBarBottomBarWrapper.dart';
import 'package:medsinway/constants.dart';
import 'package:medsinway/helper/utils.dart';

class HelpCenter extends StatelessWidget {
  static String routeName = "/help_center";
  @override
  Widget build(BuildContext context) {
    return AppBarBottomBarWrapper(
      enableGoBack: true,
      currentRoute: 'profile',
      child: Container(
        padding: EdgeInsets.all(10),
        child: Center(
          child: Card(
            borderOnForeground: true,
            semanticContainer: true,
            child: Container(
              padding: EdgeInsets.all(25),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Center(
                    child: Image.asset("assets/images/splash_logo.png",
                        height: 100.0),
                  ),
                  Text(
                    'Our Team is ready to help you',
                    style: TextStyle(fontSize: 22),
                  ),
                  // Text(
                  //   'Medsinway',
                  //   style: TextStyle(fontSize: 22, fontWeight: FontWeight.bold),
                  // ),
                  Text(
                    'Between 10 AM to 6 PM',
                    style: TextStyle(fontSize: 16),
                  ),
                  Text(
                    'Monday to Friday',
                    style: TextStyle(
                      fontSize: 16,
                      color: Colors.blueAccent,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  SizedBox(
                    height: 13,
                  ),

                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Container(
                        child: Column(
                          children: [
                            GestureDetector(
                              child: Text(
                                'Call: $kPhoneNumber',
                                style: TextStyle(
                                    color: Colors.blue,
                                    fontWeight: FontWeight.bold),
                              ),
                              onTap: () => launchURL('tel://$kPhoneNumber}'),
                            ),
                            Padding(
                              padding: const EdgeInsets.all(10),
                              child: GestureDetector(
                                child: Text(
                                  'Email: $kEmail',
                                  style: TextStyle(
                                      color: Colors.blue,
                                      fontWeight: FontWeight.bold),
                                ),
                                onTap: () =>
                                    launchURL('email://$kPhoneNumber}'),
                              ),
                            ),
                          ],
                        ),
                      ),
                      // Container(
                      //   child: SizedBox(
                      //     height: 32,
                      //     width: 32,
                      //     child: Center(
                      //       child: Ink(
                      //         decoration: const ShapeDecoration(
                      //           color: Colors.lightBlue,
                      //           shape: CircleBorder(),
                      //         ),
                      //         child: IconButton(
                      //           icon: Icon(Icons.phone_in_talk_rounded,
                      //               size: 16.0),
                      //           color: Colors.white,
                      //           onPressed: () =>
                      //               launchURL('tel://$kPhoneNumber}'),
                      //         ),
                      //       ),
                      //     ),
                      //     // child: IconButton(
                      //     //   color: Colors.red,
                      //     //   icon: Icon(
                      //     //     Icons.phone_in_talk_rounded,
                      //     //   ),
                      //     //   // SvgPicture.asset(
                      //     //   //   "assets/icons/User Icon.svg",
                      //     //
                      //     //   // ),
                      //     //   onPressed: () => _launchURL(('tel://$kPhoneNumber}')),
                      //     // ),
                      //   ),
                      // ),
                    ],
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
