import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:medsinway/components/Carousel/SimpleGridCarousel.dart';
import 'package:medsinway/constants.dart';
import 'package:medsinway/helper/Api.dart';
import 'package:medsinway/helper/utils.dart';
import 'package:medsinway/model/bannersModel.dart';

import '../../components/AppBarsWrapper/AppBarBottomBarWrapper.dart';
import '../../helper/mapIndexed.dart';

class Home extends StatefulWidget {
  static String routeName = "/";
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  static String routeName = "/";
  List<BannersModel> homeBannerList;
  List<BannersModel> dealsList;

  @override
  void initState() {
    // TODO: implement initState
    getHomePageData();
    getDeals();
    super.initState();
    Cookie('user', 'userName');
  }

  void getDeals() async {
    http.Response dealsData =
        await Api().getData('/banners', headerz: khomeDealsHeader);
    // Map ordr = jsonDecode(orders.body);
    print('homeData.body');
    print(dealsData.body);

    if (dealsData.statusCode == 200) {
      var data = jsonDecode(dealsData.body);
      var rest = data["data"] as List;

      // await setState(() {
      //   homeData = data["data"];
      // });

      print('dealsList');
      print(rest);

      setState(() {
        dealsList = rest
            .map<BannersModel>((json) => BannersModel.fromJson(json))
            .toList();
        //print(orderList[1]);
      });
      print(dealsList);
    }
  }

  void getHomePageData() async {
    http.Response homeBannerData =
        await Api().getData('/banners', headerz: khomeMainSliderHeader);
    // Map ordr = jsonDecode(orders.body);
    // print('homeData.body');
    print(homeBannerData.body);

    if (homeBannerData.statusCode == 200) {
      var data = jsonDecode(homeBannerData.body);
      var rest = data["data"] as List;

      // await setState(() {
      //   homeData = data["data"];
      // });

      // print('rest');
      // print(rest);

      setState(() {
        homeBannerList = rest
            .map<BannersModel>((json) => BannersModel.fromJson(json))
            .toList();
        //print(orderList[1]);
      });
      //print(homeBannerList);
    }
  }

  @override
  Widget build(BuildContext context) {
    return AppBarBottomBarWrapper(
      backRoute: routeName,
      enableGoBack: false,
      currentRoute: 'home',
      child: SingleChildScrollView(
        child: Column(
          children: [
            SizedBox(
              height: 5,
            ),
            SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: Row(
                children: homeBannerList != null && homeBannerList.length > 0
                    ? [
                        Row(
                          children: [
                            ...homeBannerList.mapIndexed(
                              (banner, i) => InkWell(
                                onTap: () {
                                  // Navigator.pushNamed(context, '/offer_details', arguments: id);
                                },
                                child: Container(
                                  child: Image.network(
                                    Api().AssetUrl + banner.bannerImage,
                                    fit: BoxFit.contain,
                                  ),
                                  width:
                                      MediaQuery.of(context).size.width / 1.12,
                                  height: 200,
                                  margin: EdgeInsets.symmetric(
                                      vertical: 5, horizontal: 5),
                                  padding: EdgeInsets.symmetric(vertical: 10),
                                  decoration: BoxDecoration(
                                    border: Border.all(
                                      color: Colors.blueGrey,
                                      width: 1.0,
                                    ),
                                  ),
                                ),
                              ),
                            )
                          ],
                        ),
                        //provide all the things u want to horizontally scroll here
                      ]
                    : [
                        SizedBox(
                          height: 200,
                        ),
                      ],
              ),
            ),
            // SizedBox(
            //   height: homeBannerList == null ? 10 : 0,
            // ),
            SizedBox(
              height: 10,
            ),
            Container(
              padding: EdgeInsets.symmetric(horizontal: 10),
              margin: EdgeInsets.symmetric(vertical: 0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Expanded(
                    child: RaisedButton(
                      onPressed: () =>
                          Navigator.pushNamed(context, '/upload_Rx'),
                      padding:
                          EdgeInsets.symmetric(vertical: 8, horizontal: 25),
                      color: Color(0xFF017b7e),
                      textColor: Colors.white,
                      shape: new RoundedRectangleBorder(
                          borderRadius: new BorderRadius.circular(30.0)),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Text(
                            'Upload Rx',
                            style: TextStyle(
                              fontSize: 18,
                            ),
                          ),
                          SizedBox(
                            width: 10,
                          ),
                          Icon(Icons.upload_file),
                        ],
                      ),
                    ),
                  ),
                  SizedBox(
                    width: 20,
                  ),
                  Expanded(
                    child: OutlineButton(
                      padding:
                          EdgeInsets.symmetric(vertical: 8, horizontal: 15),
                      onPressed: () => launchURL(('tel://$kPhoneNumber}')),
                      shape: new RoundedRectangleBorder(
                          borderRadius: new BorderRadius.circular(30.0)),
                      borderSide: BorderSide(
                        width: 2.0,
                        color: kAppColor,
                      ),
                      textColor: Colors.black,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Text(
                            'Tap to Order',
                            style: TextStyle(
                              fontSize: 18,
                              color: kAppColor,
                            ),
                          ),
                          SizedBox(
                            width: 10,
                          ),
                          Icon(
                            Icons.phone_in_talk_rounded,
                            color: kAppColor,
                          ),
                        ],
                      ),
                    ),
                  )
                ],
              ),
            ),
            Container(
              padding: EdgeInsets.only(top: 10),
              child: Text(
                'Deals of the day',
                style: TextStyle(
                  fontSize: 16,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
            Container(
              child: dealsList == null
                  ? null
                  : SingleChildScrollView(
                      scrollDirection: Axis.horizontal,
                      child: Row(
                        children: [
                          SimpleGridCarousel(dealsList),
                          //provide all the things u want to horizontally scroll here
                        ],
                      ),
                    ),
            )
          ],
        ),
      ),
    );
  }
}
