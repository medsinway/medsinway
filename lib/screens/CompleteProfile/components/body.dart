import 'package:flutter/material.dart';

import '../../../constants.dart';
import '../../../size_config.dart';
import 'complete_profile_form.dart';

class Body extends StatefulWidget {
  @override
  _BodyState createState() => _BodyState();
}

class _BodyState extends State<Body> {
  @override
  Widget build(BuildContext context) {
    Dimensions(context);
    return SafeArea(
      child: SizedBox(
        width: double.infinity,
        child: Padding(
          padding: EdgeInsets.symmetric(
              horizontal: getProportionateScreenWidthCtx(20, context)),
          child: SingleChildScrollView(
            child: Column(
              children: [
                SizedBox(height: Dimensions.boxHeight * 0.03),
                Text("Profile", style: headingStyle),
                // Text(
                //   "Complete your details or continue  \nwith social media",
                //   textAlign: TextAlign.center,
                // ),
                SizedBox(height: Dimensions.boxHeight * 0.06),
                CompleteProfileForm(),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
