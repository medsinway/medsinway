import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:medsinway/components/Forms/InputTextField.dart';
import 'package:medsinway/helper/user.dart';

// import 'package:shop_app/screens/otp/otp_screen.dart';

import '../../../components/Buttons/DefaultButton.dart';
import '../../../components/Forms/FormError.dart';
import '../../../components/Icons/CustomSurfixIcon.dart';
import '../../../constants.dart';
import '../../../size_config.dart';

class CompleteProfileForm extends StatefulWidget {
  @override
  _CompleteProfileFormState createState() => _CompleteProfileFormState();
}

class _CompleteProfileFormState extends State<CompleteProfileForm> {
  final _formKey = GlobalKey<FormState>();
  final List<String> errors = [];
  String firstName;
  String lastName;
  String phoneNumber;
  String address;
  Map userData;
  @override
  void initState() {
    // TODO: implement initState

    // if (userData != null) {
    //   print('User().loadUserData()');
    //   print(userData.name);
    // }loadUserData
    getUser();
    // print(userData['name']);
    // setState(() {
    //   userData = User().loadUserData();
    // });
    super.initState();
  }

  getUser() async {
    final String responseJson = await User().loadUserData();

    setState(() {
      userData = jsonDecode(responseJson);
    });
  }

  void addError({String error}) {
    if (!errors.contains(error))
      setState(() {
        errors.add(error);
      });
  }

  void removeError({String error}) {
    if (errors.contains(error))
      setState(() {
        errors.remove(error);
      });
  }

  @override
  Widget build(BuildContext context) {
    return userData != null
        ? Form(
            key: _formKey,
            child: Column(
              children: [
                buildTextNumberFormField(
                    type: 'text',
                    label: 'Name',
                    initValue: userData['name'],
                    inputName: 'name'),
                // buildFirstNameFormField(),

                SizedBox(height: getProportionateScreenHeightCtx(30, context)),
                buildTextNumberFormField(
                    type: 'email',
                    label: 'Email',
                    initValue: userData['email'],
                    inputName: 'Name'),
                SizedBox(height: getProportionateScreenHeightCtx(30, context)),
                buildTextNumberFormField(
                    type: 'number',
                    label: 'Phone number',
                    initValue: 'Phone number',
                    inputName: 'PhoneNumber'),
                SizedBox(height: getProportionateScreenHeightCtx(30, context)),
                buildTextNumberFormField(
                    type: 'text', initValue: 'Address', inputName: 'Address'),
                FormError(errors: errors),
                SizedBox(height: getProportionateScreenHeightCtx(40, context)),

                Text(
                  kPrivacyPolicy,
                  textAlign: TextAlign.center,
                  style: Theme.of(context).textTheme.caption,
                ),
                SizedBox(height: getProportionateScreenWidthCtx(20, context)),
                DefaultButton(
                  text: "Submit",
                  press: () {
                    if (_formKey.currentState.validate()) {
                      Navigator.pushNamed(context, '/'
                          // OtpScreen.routeName
                          );
                    }
                  },
                ),
              ],
            ),
          )
        : Center(
            child: Text('Loading....'),
          );
  }

  TextFormField buildAddressFormField() {
    return TextFormField(
      onSaved: (newValue) => address = newValue,
      onChanged: (value) {
        if (value.isNotEmpty) {
          removeError(error: kAddressNullError);
        }
        return null;
      },
      validator: (value) {
        if (value.isEmpty) {
          addError(error: kAddressNullError);
          return "";
        }
        return null;
      },
      decoration: InputDecoration(
        labelText: "Address",
        hintText: "Enter your phone address",
        // If  you are using latest version of flutter then lable text and hint text shown like this
        // if you r using flutter less then 1.20.* then maybe this is not working properly
        floatingLabelBehavior: FloatingLabelBehavior.always,
        suffixIcon:
            CustomSurffixIcon(svgIcon: "assets/icons/Location point.svg"),
      ),
    );
  }

  TextFormField buildPhoneNumberFormField() {
    return TextFormField(
      keyboardType: TextInputType.phone,
      onSaved: (newValue) => phoneNumber = newValue,
      onChanged: (value) {
        if (value.isNotEmpty) {
          removeError(error: kPhoneNumberNullError);
        }
        return null;
      },
      validator: (value) {
        if (value.isEmpty) {
          addError(error: kPhoneNumberNullError);
          return "";
        }
        return null;
      },
      decoration: InputDecoration(
        labelText: "Phone Number",
        hintText: "Enter your phone number",
        // If  you are using latest version of flutter then lable text and hint text shown like this
        // if you r using flutter less then 1.20.* then maybe this is not working properly
        floatingLabelBehavior: FloatingLabelBehavior.always,
        suffixIcon: CustomSurffixIcon(svgIcon: "assets/icons/Phone.svg"),
      ),
    );
  }

  TextFormField buildLastNameFormField() {
    return TextFormField(
      onSaved: (newValue) => lastName = newValue,
      initialValue: '',
      decoration: InputDecoration(
        labelText: "Last Name",
        hintText: "Enter your last name",
        // If  you are using latest version of flutter then lable text and hint text shown like this
        // if you r using flutter less then 1.20.* then maybe this is not working properly
        floatingLabelBehavior: FloatingLabelBehavior.always,
        suffixIcon: CustomSurffixIcon(svgIcon: "assets/icons/User.svg"),
      ),
    );
  }

  TextFormField buildFirstNameFormField() {
    return TextFormField(
      onSaved: (newValue) => firstName = newValue,
      onChanged: (value) {
        if (value.isNotEmpty) {
          removeError(error: kNamelNullError);
        }
        return null;
      },
      validator: (value) {
        if (value.isEmpty) {
          addError(error: kNamelNullError);
          return "";
        }
        return null;
      },
      decoration: InputDecoration(
        labelText: "First Name",
        hintText: "Enter your first name",
        // If  you are using latest version of flutter then lable text and hint text shown like this
        // if you r using flutter less then 1.20.* then maybe this is not working properly
        floatingLabelBehavior: FloatingLabelBehavior.always,
        suffixIcon: CustomSurffixIcon(svgIcon: "assets/icons/User.svg"),
      ),
    );
  }
}
