import 'package:flutter/material.dart';

import '../../components/AppBarsWrapper/AppBarBottomBarWrapper.dart';
import 'components/body.dart';

class CompleteProfileScreen extends StatelessWidget {
  static String routeName = "/complete_profile";
  @override
  Widget build(BuildContext context) {
    return AppBarBottomBarWrapper(
      enableGoBack: true,
      currentRoute: 'profile',
      child: Body(),
    );
  }
}
