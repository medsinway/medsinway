import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:medsinway/constants.dart';
import 'package:medsinway/helper/utils.dart';

import '../../size_config.dart';

class NotVerifiedUser extends StatefulWidget {
  static String routeName = "/not_verified_user";
  @override
  _NotVerifiedUserState createState() => _NotVerifiedUserState();
}

class _NotVerifiedUserState extends State<NotVerifiedUser> {
  onWillPop(context) async {
    SystemChannels.platform.invokeMethod('SystemNavigator.pop');
    return false;
  }

  @override
  Widget build(BuildContext context) {
    // You have to call it on your starting screen
    SizeConfig().init(context);
    return Scaffold(
        // body: DoubleBackToCloseWidget(child: Body()),
        body: WillPopScope(
            onWillPop: () => onWillPop(context),
            child: Body(onWillPop, context)) //Other widgets here)

        );
  }
}

Container Body(onWillPop, context) {
  return Container(
    padding: EdgeInsets.all(10),
    child: Center(
      child: Card(
        borderOnForeground: true,
        semanticContainer: true,
        child: Container(
          padding: EdgeInsets.all(25),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  IconButton(
                    icon: Icon(
                      Icons.close,
                    ),
                    onPressed: () => onWillPop(context),
                  ),
                ],
              ),
              Center(
                child:
                    Image.asset("assets/images/splash_logo.png", height: 100.0),
              ),
              Text(
                'Congratulations! Welcome to',
                style: TextStyle(fontSize: 22),
              ),
              Text(
                'Medsinway',
                style: TextStyle(fontSize: 22, fontWeight: FontWeight.bold),
              ),
              Text(
                'Relax! Account verification is in process',
                style: TextStyle(fontSize: 16),
              ),
              SizedBox(
                height: 10,
              ),
              SizedBox(
                height: 20,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    child: FlatButton(
                      child: Text(
                        'Call: $kPhoneNumber',
                        style: TextStyle(
                            color: Colors.blue, fontWeight: FontWeight.bold),
                      ),
                      onPressed: () => launchURL('tel://$kPhoneNumber}'),
                    ),
                  ),
                  Container(
                    child: SizedBox(
                      height: 32,
                      width: 32,
                      child: Center(
                        child: Ink(
                          decoration: const ShapeDecoration(
                            color: Colors.lightBlue,
                            shape: CircleBorder(),
                          ),
                          child: IconButton(
                            icon: Icon(Icons.phone_in_talk_rounded, size: 16.0),
                            color: Colors.white,
                            onPressed: () => launchURL('tel://$kPhoneNumber}'),
                          ),
                        ),
                      ),
                      // child: IconButton(
                      //   color: Colors.red,
                      //   icon: Icon(
                      //     Icons.phone_in_talk_rounded,
                      //   ),
                      //   // SvgPicture.asset(
                      //   //   "assets/icons/User Icon.svg",
                      //
                      //   // ),
                      //   onPressed: () => _launchURL(('tel://$kPhoneNumber}')),
                      // ),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    ),
  );
}
