import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:medsinway/helper/user.dart';
import 'package:medsinway/screens/CompleteProfile/complete_profile_screen.dart';
import 'package:medsinway/screens/HelpCenter/HelpCenter.dart';
import 'package:medsinway/screens/OfferUpload/OfferUpload.dart';
import 'package:medsinway/screens/Orders/Orders.dart';
import 'package:medsinway/screens/SignIn/sign_in_screen.dart';

import 'ProfileMenu.dart';
import 'ProfilePic.dart';

class Body extends StatefulWidget {
  @override
  _BodyState createState() => _BodyState();
}

class _BodyState extends State<Body> {
  bool isLogOut;
  bool isAdmin;

  @override
  void initState() {
// TODO: implement initState
    user();
    super.initState();
  }

  user() async {
    var user = jsonDecode(await User().loadUserData());
    print(user['role_id']);
    if (user['role_id'] == 1) {
      setState(() {
        isAdmin = true;
      });
    }
  }

  void handleLogOut() async {
    if (await User().logout()) {
      setState(() {
        isLogOut = true;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      padding: EdgeInsets.symmetric(vertical: 20),
      child: Column(
        children: [
          ProfilePic(),
          SizedBox(height: 20),
          ProfileMenu(
            text: "My Account",
            icon: Icons.person, //"assets/icons/User Icon.svg",
            press: () =>
                Navigator.pushNamed(context, CompleteProfileScreen.routeName),
          ),
          ProfileMenu(
            text: "My Orders",
            icon: Icons.reorder, //"assets/icons/User Icon.svg",
            press: () => Navigator.pushNamed(context, Orders.routeName),
          ),
          SizedBox(
              child: isAdmin != null
                  ? ProfileMenu(
                      text: "Offers Upload",
                      icon: Icons.local_offer_sharp,
                      press: () => Navigator.pushNamed(context,
                          OfferUpload.routeName), //"assets/icons/Bell.svg",
                      //press: () {/offers_upload},
                    )
                  : null),
          ProfileMenu(
            text: "Notifications (coming soon)",
            icon: Icons
                .notification_important_outlined, //"assets/icons/Bell.svg",
            press: () {},
          ),
          // ProfileMenu(
          //   text: "Settings",
          //   icon: Icons.settings, //"assets/icons/Settings.svg",
          //   press: () {},
          // ),
          ProfileMenu(
            text: "Help Center",
            icon: Icons.help_center_sharp, // "assets/icons/Question mark.svg",
            press: () => Navigator.pushNamed(context, HelpCenter.routeName),
          ),
          ProfileMenu(
            text: "Log Out",
            icon: Icons.logout, //"assets/icons/Log out.svg",
            press: () {
              handleLogOut();
              isLogOut ??
                  Navigator.pushAndRemoveUntil(
                      context,
                      MaterialPageRoute(
                          builder: (BuildContext context) =>
                              SignInScreen('hideBack')),
                      ModalRoute.withName('/'));
            },
          ),
        ],
      ),
    );
  }
}
