import 'package:flutter/material.dart';

import '../../components/AppBarsWrapper/AppBarBottomBarWrapper.dart';
import 'components/body.dart';

class Profile extends StatelessWidget {
  static String routeName = "/profile";
  @override
  Widget build(BuildContext context) {
    return AppBarBottomBarWrapper(
      enableGoBack: true,
      currentRoute: 'profile',
      child: Body(),
    );
  }
}
